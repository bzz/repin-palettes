//
//  PaletteCell.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 19/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "PaletteCell.h"

@implementation PaletteCell

- (void)prepareForReuse {
	[super prepareForReuse];
	self.layer.borderWidth = 0;
}



- (id)init
{
	if (self=[super init]) {
		[self setupInit];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ([super initWithCoder:aDecoder]) {
		[self setupInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if ([super initWithFrame:frame]) {
		[self setupInit];
    }
    return self;
}

- (void)setupInit
{
}


- (void)setColor:(UIColor *)color
{
	_color = color;
	self.backgroundColor = color;
}

@end
