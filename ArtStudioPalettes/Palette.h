//
//  Palette.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 06/12/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Picture.h"
#import "Color.h"



@interface Palette : NSObject

@property (nonatomic, strong) NSMutableArray *array;


- (instancetype)initWithPicture:(Picture *)picture;

- (void)sortByDistanceFromColor:(Color *)color;
- (NSMutableArray *)sort;
///- (UIImage *)uiimage;

@end
