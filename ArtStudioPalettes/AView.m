//
//  AView.m
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/19/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "AView.h"
#import "Model.h"

@import CoreGraphics;
@import QuartzCore;

@interface AView ()
{
	Model *model;
}
@property (nonatomic, strong) NSString *documentsDirectory;

@end

@implementation AView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		self.frame = frame;
		[self doInit];
	}
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self doInit];
	}
	return self;
}

- (id)init
{
    self = [super init];
	if (self) {
		[self doInit];
	}
	return self;
}

- (void)doInit {
	model = [Model sharedInstance];
}


#pragma mark
#pragma mark
#pragma mark

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
	self.image = [UIImage imageWithData:model.pictureAreaData forImage:self.image];
	CGContextRef context = UIGraphicsGetCurrentContext();
	if (context) {
		CGContextTranslateCTM(context, 0, self.image.size.height);
		CGContextScaleCTM(context, 1.0f, -1.0f);
		CGContextDrawImage(context, CGRectMake(0, 0, self.image.size.width, self.image.size.height), self.image.CGImage);
	}
}




- (void)setImage:(UIImage *)image
{
	_image = image;
	[self setNeedsDisplay];
}



#pragma mark
#pragma mark
#pragma mark




@end
