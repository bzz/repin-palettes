//
//  AddNewColorViewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 17/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewColorViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UISlider *redSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueSlider;
@property (weak, nonatomic) IBOutlet UISlider *opacitySlider;

@end
