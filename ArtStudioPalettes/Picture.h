//
//  Picture.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 06/12/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Palette;
@interface Picture : NSObject

@property (nonatomic) NSInteger width;
@property (nonatomic) NSInteger height;
@property (nonatomic, strong) NSMutableArray *lines;

//non-cached
@property (nonatomic, strong) Palette *palette;



- (instancetype)initWithImage:(UIImage *)image;

- (UIImage *)uiimage;



- (Picture *)invert;
- (Picture *)upscale2Neighbour;
- (Picture *)upscale3Neighbour;
- (Picture *)upscaleBy:(NSUInteger)times;

- (Picture *)downscale2;
- (Picture *)downscale3;


@end
