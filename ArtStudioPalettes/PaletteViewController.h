//
//  PaletteViewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 17/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
#import "ColorBytePicker.h"

@interface PaletteViewController : UIViewController
<UICollectionViewDataSource,
UICollectionViewDelegate,
UIAlertViewDelegate,
ColorBytePickerDelegate
>


@property (weak, nonatomic) IBOutlet UIImageView *pictureView;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UICollectionView *paletteCollectionView;
@property (weak, nonatomic) IBOutlet ColorBytePicker *redSlider;
@property (weak, nonatomic) IBOutlet ColorBytePicker *greenSlider;
@property (weak, nonatomic) IBOutlet ColorBytePicker *blueSlider;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *savePaletteButton;
@property (weak, nonatomic) IBOutlet UIButton *lockButton;



@property (nonatomic, strong) UIColor *selectedColor;

@end
