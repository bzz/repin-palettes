//
//  Palette.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 06/12/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "Palette.h"
#import "Color.h"


@implementation Palette

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self setupDefaults];
	}
	return self;
}

- (instancetype)initWithPicture:(Picture *)picture
{
	[self setupDefaults];
	for (NSMutableArray *line in picture.lines) {
		for (Color *c in line) {
			BOOL passed = YES;
			for (Color *pc in self.array) {
				if ([pc isEqual:c]) {
					passed = NO;
				}
			}
			if (passed) {
				[self.array addObject:c];
				NSLog(@"%@", c.description);
			}
		}
	}
	return self;
}

- (void)setupDefaults {
	self.array = [NSMutableArray new];
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
	if (self) {
		[self setupDefaults];
		self.array = [coder decodeObjectForKey:@"array"];
	}
	return self;
}


- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:self.array forKey:@"array"];
}


- (void)sortByDistanceFromColor:(Color *)color
{
	self.array = [self.array sortedArrayUsingComparator:^NSComparisonResult(Color* c1, Color* c2)
	{
		NSNumber *first = @([c1 distanceFromColor:color]);
		NSNumber *second = @([c2 distanceFromColor:color]);
		return [first compare:second];
	}].mutableCopy;
}

- (NSMutableArray *)sort
{
	NSMutableArray *arr = [self.array sortedArrayUsingComparator:^NSComparisonResult(Color* c1, Color* c2)
				  {
					  NSNumber *first = @([c1 placeInRow]);
					  NSNumber *second = @([c2 placeInRow]);
					  return [first compare:second];
				  }].mutableCopy;
	return arr;
}



@end
