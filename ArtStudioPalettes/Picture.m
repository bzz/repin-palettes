//
//  Picture.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 06/12/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "Picture.h"
#import "Color.h"
#import "Palette.h"

@implementation Picture


- (instancetype)init
{
	self = [super init];
	if (self) {
		[self setupDefaults];
	}
	return self;
}

- (void)setupDefaults {
	self.width = 0;
	self.height = 0;
	self.lines = [NSMutableArray new];
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
	if (self) {
		[self setupDefaults];
		self.width = [coder decodeIntegerForKey:@"width"];
		self.height = [coder decodeIntegerForKey:@"height"];
		self.lines = [coder decodeObjectForKey:@"lines"];
	}
	return self;
}


- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeInteger:self.width forKey:@"width"];
	[coder encodeInteger:self.height forKey:@"height"];
	[coder encodeObject:self.lines forKey:@"lines"];
}


- (instancetype)initWithImage:(UIImage *)image
{
	self.width = image.size.width;
	self.height = image.size.height;
	NSData *data = [NSMutableData dataFromImage:image];
	Byte* rawData = (Byte*)[data bytes];
	size_t it = 0;
	
	self.lines = [NSMutableArray new];
	for (size_t line = 0; line < self.height; ++line) {
		NSMutableArray *line = [NSMutableArray new];
		for (size_t i = 0; i < self.width; ++i) {
			Color *c = [[Color alloc]initWithRed:rawData[it] green:rawData[it+1] blue:rawData[it+2] alpha:rawData[it+3]];
			[line addObject:c];
			it += 4;
		}
		[self.lines addObject:line];
	}
	return self;
}


- (NSMutableData *)data
{
	size_t it = 0;
	
	NSUInteger dlength = sizeof(unsigned char) * _height * _width * 4;
	NSMutableData *data = [NSMutableData dataWithLength:dlength];
	unsigned char *rawData = [data mutableBytes];
	
	for (size_t line = 0; line < self.height; ++line) {
		for (size_t i = 0; i < self.width; ++i) {
			Color *c = self.lines[line][i];
//			NSLog(@"%i %i %i", c.red, c.green, c.blue);
			rawData[it] = (Byte)c.red;
			rawData[it+1] = (Byte)c.green;
			rawData[it+2] = (Byte)c.blue;
			rawData[it+3] = (Byte)c.alpha;
			it += 4;
		}
	}
	data = [NSMutableData dataWithBytes:(const void *)rawData length:dlength];
	return data;
}

- (Palette *)palette
{
	if (!_palette) {
		_palette = [[Palette alloc]initWithPicture:self];
	}
	return _palette;
}



- (UIImage *)uiimage
{
	UIImage* image = [UIImage imageWithColor:[UIColor redColor] andSize:CGSizeMake(_width, _height)];
	return [UIImage imageWithData:self.data forImage:image];
	/*
	 UIImage* image = [UIImage imageWithColor:[UIColor redColor] andSize:CGSizeMake(_width, _height)];
	 if (self.data && image) {
		CGImageRef imageRef = [image CGImage];
		NSUInteger width = CGImageGetWidth(imageRef);
		NSUInteger height = CGImageGetHeight(imageRef);
		
		unsigned char *rawData = [self.data mutableBytes];
		CGContextRef context;
		context = CGBitmapContextCreate(rawData, width, height, 8, 4*width, CGImageGetColorSpace(imageRef), kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
		imageRef = CGBitmapContextCreateImage(context);
		image = [UIImage imageWithCGImage:imageRef];
		CGContextRelease(context);
	 }
	 return image;
	 */
}

- (Picture *)invert
{
	for (size_t line = 0; line < self.height; ++line) {
		for (size_t i = 0; i < self.width; ++i) {
			Color *c = self.lines[line][i];
			c.red = 255 - c.red;
			c.green = 255 - c.green;
			c.blue = 255 - c.blue;
			self.lines[line][i] = c;
		}
	}
	return self;
}

- (Picture *)upscale2Neighbour
{
	self.height *= 2;
	self.width *= 2;

	for (size_t line = 0; line < self.height; ++line) {
		for (size_t i = 0; i < self.width; ++i) {
			Color *c = self.lines[line][i];
			[self.lines[line] insertObject:c atIndex:1+i++];
		}
		NSMutableArray *lineArr = self.lines[line];
		[self.lines insertObject:lineArr atIndex:1+line++];
	}
	return self;
}

- (Picture *)upscale3Neighbour
{
	self.height *= 3;
	self.width *= 3;
	
	for (size_t line = 0; line < self.height; ++line) {
		for (size_t i = 0; i < self.width; ++i) {
			Color *c = self.lines[line][i];
			[self.lines[line] insertObject:c atIndex:1+i++];
			[self.lines[line] insertObject:c atIndex:1+i++];
		}
		NSMutableArray *lineArr = self.lines[line];
		[self.lines insertObject:lineArr atIndex:1+line++];
		[self.lines insertObject:lineArr atIndex:1+line++];
	}
	return self;
}

- (Picture *)upscaleBy:(NSUInteger)times {
	self.height *= times;
	self.width *= times;
	
	for (size_t line = 0; line < self.height; ++line) {
		for (size_t i = 0; i < self.width; ++i) {
			Color *c = self.lines[line][i];
			switch (times) {
				case 5:
					[self.lines[line] insertObject:c atIndex:1+i++];
				case 4:
					[self.lines[line] insertObject:c atIndex:1+i++];
				case 3:
					[self.lines[line] insertObject:c atIndex:1+i++];
				case 2:
					[self.lines[line] insertObject:c atIndex:1+i++];
				case 1:
				case 0:
				default:
					break;
			}
		}
		NSMutableArray *lineArr = self.lines[line];
		switch (times) {
			case 5:
				[self.lines insertObject:lineArr atIndex:1+line++];
			case 4:
				[self.lines insertObject:lineArr atIndex:1+line++];
			case 3:
				[self.lines insertObject:lineArr atIndex:1+line++];
			case 2:
				[self.lines insertObject:lineArr atIndex:1+line++];
			case 1:
			case 0:
			default:
				break;
		}
	}
	return self;
}



@end
