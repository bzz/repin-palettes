//
//  Color.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 06/12/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "Color.h"

@implementation Color

- (instancetype)init
{
	self = [super init];
	if (self) {
		[self setupDefaults];
	}
	return self;
}

- (instancetype)initWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha
{
	self = [super init];
	if (self) {
//		[self setupDefaults];
		self.red = red;
		self.green = green;
		self.blue = blue;
		self.alpha = alpha;
	}
	return self;
}



- (void)setupDefaults {
	self.red = 0;
	self.green = 0;
	self.blue = 0;
	self.alpha = 255;
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
	if (self) {
		[self setupDefaults];
		self.red = [coder decodeIntegerForKey:@"red"];
		self.green = [coder decodeIntegerForKey:@"green"];
		self.blue = [coder decodeIntegerForKey:@"blue"];
		self.alpha = [coder decodeIntegerForKey:@"alpha"];
	}
	return self;
}


- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeInteger:self.red forKey:@"red"];
	[coder encodeInteger:self.green forKey:@"green"];
	[coder encodeInteger:self.blue forKey:@"blue"];
	[coder encodeInteger:self.alpha forKey:@"alpha"];
}



- (BOOL)isEqual:(id)object
{
	if ([object isKindOfClass:[Color class]]) {
		Color *obj = (Color *)object;
		if (obj.red == _red &&
			obj.green == _green &&
			obj.blue == _blue &&
			obj.alpha == _alpha) {
			return YES;
		}
	}
	return NO;
}



- (UIColor *)uicolor
{
	return [UIColor colorWithRed:(CGFloat)_red / 255 green:(CGFloat)_green / 255 blue:(CGFloat)_blue / 255 alpha:(CGFloat)_alpha / 255];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"[COLOR R:%03i G:%03i B:%03i A:%03i] place:%i", _red, _green, _blue, _alpha, self.placeInRow];
}

- (NSInteger)distanceFromColor:(Color *)color
{
	NSInteger a = 299;
	NSInteger b = 144;
	NSInteger c = 587;
	
	NSInteger distance = (a * (_red - color.red)^2 + b * (_green - color.green)^2 + c * (_blue - color.blue)^2);
	return distance;
}




typedef NS_ENUM(NSInteger, ColorSp) {
	Red        =1,
	RedGreen   =2,  //yellow
	Green      =3,
	GreenBlue  =4,  //cyan
	Blue       =5,
	BlueRed    =6,  //violet
	DarkGray   =7,
	LightGray  =8
};


- (NSInteger)placeInRow
{
	NSInteger rgdiff = _red - _green;
	NSInteger rbdiff = _red - _blue;
	NSInteger gbdiff = _green - _blue;
	
	ColorSp place = 0;
	
	
	NSInteger max;
	NSInteger mid;
	NSInteger min;
	ColorSp maxC;
	ColorSp midMixC;
	
	if (_red >= _green && _red >= _blue) {
		max = _red;
		maxC = Red;
		if (_green > _blue) {
			mid = _green;
			min = _blue;
			midMixC = RedGreen;
		} else {
			mid = _blue;
			min = _green;
			midMixC = BlueRed;
		}
	} else if (_green >= _red && _green >= _blue) {
		max = _green;
		maxC = Green;
		if (_red > _blue) {
			mid = _red;
			min = _blue;
			midMixC = RedGreen;
		} else {
			mid = _blue;
			min = _red;
			midMixC = GreenBlue;
		}
	} else if (_blue >= _red && _blue >= _green) {
		max = _blue;
		maxC = Blue;
		if (_red > _green) {
			mid = _red;
			min = _green;
			midMixC = BlueRed;
		} else {
			mid = _green;
			min = _red;
			midMixC = GreenBlue;
		}
	}
	
	if (max - mid > mid - min) {
		place = maxC;
	} else {
		place = midMixC;
	}
	
//	if (abs(abs(max - mid) - abs(max - min)) < 10) {
//		place = DarkGray;
//	}
	
		
	/*
	if (_red > _green && _red > _blue) {             //max red
		place = Red;
		if (_green >= _blue) {					//mid green
			if (rgdiff < gbdiff) {
				place = Yellow;
			}
		} else {								//mid blue
			if (rbdiff < -gbdiff) {
				place = Violet;
			}
		}
		
		
	} else if (_green > _red && _green > _blue) {    //max green
		place = Green;
		if (_red >= _blue) {					//mid red
			if (-rgdiff < rbdiff) {
				place = Yellow;
			}
		} else {								//mid blue
			if (-rgdiff < -rbdiff) {
				place = Cyan;
			}
		}

	
	} else if (_blue > _red && _blue > _green) {     //max blue
		place = Blue;
		if (_red >= _green) {					//mid red
			if (-rbdiff < rgdiff) {
				place = Violet;
			}
		} else {								//mid green
			if (-rbdiff < -rgdiff) {
				place = Cyan;
			}
		}
	}
	*/
	
	
	return place;
}



@end
