//
//  ProjectCell.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 10/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "ProjectCell.h"
#import "Model.h"

@implementation ProjectCell

- (void)prepareForReuse {
	[super prepareForReuse];
	self.deleteButton.hidden = NO;
}



- (id)init {
	if (self=[super init]) {
//		[self setupInit];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ([super initWithCoder:aDecoder]) {
//		[self setupInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
//		[self setupInit];
    }
    return self;
}

//- (void)setupInit {
//}






@end
