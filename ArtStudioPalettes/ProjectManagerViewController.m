//
//  ProjectManagerViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 09/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "ProjectManagerViewController.h"
#import "FileManagerViewController.h"

@interface ProjectManagerViewController () {
	Model *model;
	UIImage *emptyImage;
	NSString *fileNameToPass;
}


@property (weak, nonatomic) IBOutlet UICollectionView *projectsList;
@property (strong, nonatomic) NSMutableArray *projectNamesArray;
@property (strong, nonatomic) NSMutableArray *iconsArray;



@end

@implementation ProjectManagerViewController

#pragma mark
#pragma mark
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
	model = [Model sharedInstance];
	emptyImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(40, 40)];  //testtest
}


#pragma mark
#pragma mark
#pragma mark LIFE
- (void)viewDidAppear:(BOOL)animated {
	[self formProjectNamesArray];
	[self.projectsList reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.projectsList.dataSource = self;
	self.projectsList.delegate = self;
	UIBarButtonItem *newButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"new.png"] style:UIBarButtonItemStylePlain target:self action:@selector(newProjectButtonPressed)];
	self.navigationItem.rightBarButtonItem = newButton;
	[self formProjectNamesArray];

	UIImage *pal = [UIImage imageWithContentsOfFile:[[NSUserDefaults standardUserDefaults] valueForKey:@"palette"]];
	if (pal) {
		[model formPaletteUserColorsArrayFromImage:pal];
	} else {
		[self performSegueWithIdentifier:@"toPalettes" sender:self];
	};
}



- (void)formProjectNamesArray {
	if (self.projectNamesArray) {
		[self.projectNamesArray removeAllObjects];
		[self.iconsArray removeAllObjects];
	} else 	{
		self.projectNamesArray = [NSMutableArray new];
		self.iconsArray = [NSMutableArray new];
	}
	
	NSArray *array = [model.fileManager contentsOfDirectoryAtPath:model.documentsDirectory error:nil];
	if (array.count) {
		[self addProject:@"/" image:emptyImage];
		for (NSString *file in array) {
			BOOL isDirectory = NO;
			if ([model.fileManager fileExistsAtPath:[model.documentsDirectory stringByAppendingPathComponent:file] isDirectory:&isDirectory]) {
				if (isDirectory) {
					[self addProject:file image:emptyImage];
				}
			}
		}
	}
	array = nil;
}

- (void)addProject:(NSString*)name image:(UIImage*)image {
	[self.projectNamesArray addObject:name];
	[self.iconsArray addObject:image];
}






#pragma mark
#pragma mark
#pragma mark Delegates
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
    ProjectCell *cell = (ProjectCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
	cell.pictureView.image = self.iconsArray[indexPath.row];
	cell.projectNameField.text = self.projectNamesArray[indexPath.row];
	NSInteger filesCount = [self countFilesAtPath:[model.documentsDirectory stringByAppendingPathComponent:cell.projectNameField.text]];
	cell.filesCountLabel.text = filesCount == 1? @"1 file" :[NSString stringWithFormat:@"%li files", (long)filesCount];
		
	cell.deleteButton.tag = indexPath.row;
	
    cell.projectNameField.userInteractionEnabled = NO;
	if ([cell.projectNameField.text isEqualToString:@"/"]) {
		cell.deleteButton.hidden = YES;
		cell.filesCountLabel.text = nil;
	}

	return cell;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.projectNamesArray.count;
}

- (NSInteger)countFilesAtPath:(NSString*)path {
	NSArray *array = [model.fileManager contentsOfDirectoryAtPath:path error:nil];
	NSInteger count = array.count;
	for (path in array) {
		BOOL isDirectory = NO;
		if ([model.fileManager fileExistsAtPath:[model.documentsDirectory stringByAppendingPathComponent:path] isDirectory:&isDirectory]) {
			if (isDirectory) {
				count--;
			}
		}
	}
	return count;
}


#pragma mark
#pragma mark
#pragma mark ACTIONS

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	ProjectCell *cell = (ProjectCell*)[collectionView cellForItemAtIndexPath:indexPath];
	fileNameToPass = cell.projectNameField.text;
	[self performSegueWithIdentifier:@"toFileManager" sender:self];
}


- (IBAction)deleteButtonPressed:(UIButton *)sender {
	if (sender.selected) {
		sender.selected = NO;
		[self deleteDirAtRow:sender.tag];
	} else {
		sender.backgroundColor = [sender.backgroundColor inverseColor];
		sender.selected = YES;
		[sender performSelector:@selector(setBackgroundColor:) withObject:[sender.backgroundColor inverseColor] afterDelay:1.0];
		[sender performSelector:@selector(setSelected:) withObject:@"NO" afterDelay:1.0];
	}
}
- (void)deleteDirAtRow:(NSInteger)row {
	[self.self.projectsList performBatchUpdates:^{
		if (row < self.projectNamesArray.count) {
			[model.fileManager removeItemAtPath:[model.documentsDirectory stringByAppendingPathComponent:self.projectNamesArray[row]] error:nil];
		}
		[self.projectNamesArray removeObjectAtIndex:row];
		NSIndexPath *indexPath =[NSIndexPath indexPathForRow:row inSection:0];
		[self.projectsList deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
		[self.projectsList reloadData];
    }];
}

- (void)newProjectButtonPressed {
	[self createNewFolder];
	model.picture = emptyImage;
	[self formProjectNamesArray];
	[self.projectsList reloadData];
	[self.projectsList setContentOffset:CGPointMake(0, self.projectsList.contentSize.height - self.projectsList.frame.size.height + 150) animated:YES];
	
	[self performSegueWithIdentifier:@"toFileManager" sender:self];

}
- (void)createNewFolder {
	NSInteger i = 1;
	NSString *projectName = [NSString stringWithFormat:@"Project%02i",i];
	NSString *dataPath = [model.documentsDirectory stringByAppendingPathComponent:projectName];
	while ([model.fileManager fileExistsAtPath:dataPath]) {
		i++;
		projectName = [NSString stringWithFormat:@"Project%02i",i];
		dataPath = [model.documentsDirectory stringByAppendingPathComponent:projectName];
	}
	[model.fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
	fileNameToPass = projectName;
}

- (IBAction)loadButtonPressed:(UIBarButtonItem *)sender {
	UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
	imagePicker.delegate = self;
	imagePicker.allowsEditing = YES;
	imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	
	[self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	[picker dismissViewControllerAnimated:YES completion:^{
		[self createNewFolder];
		[self formProjectNamesArray];
		[self.projectsList reloadData];
		[self.projectsList setContentOffset:CGPointMake(0, self.projectsList.contentSize.height - self.projectsList.frame.size.height + 150) animated:YES];
		
		[self performSegueWithIdentifier:@"toFileManager" sender:self];
		model.picture = info[UIImagePickerControllerEditedImage];
	}];
}


#pragma mark
#pragma mark
#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString: @"toFileManager"]) {
		FileManagerViewController *fileManagerVC = segue.destinationViewController;
		fileManagerVC.filesDirectory = [model.documentsDirectory stringByAppendingPathComponent:fileNameToPass];
	}
}


@end
