//
//  TransformationsViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 13/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "TransformationsViewController.h"
#import "Model.h"

@interface TransformationsViewController ()
{
	Model *model;
	CGRect initialInsideFrame;
	int pixelsLeft;
	int pixelsRight;
	int pixelsTop;
	int pixelsBottom;
}

@property (strong, nonatomic) UIView *view1;
@property (strong, nonatomic) UIView *view2;
@property (strong, nonatomic) UIView *view3;
@property (strong, nonatomic) UIView *view4;
@property (strong, nonatomic) UIView *pin1;
@property (strong, nonatomic) UIView *pin2;
@property (strong, nonatomic) UIView *pin3;
@property (strong, nonatomic) UIView *pin4;
@property (weak, nonatomic) IBOutlet UIView *pinBoard;




@end

@implementation TransformationsViewController

#pragma mark
#pragma mark
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit
{
	model = [Model sharedInstance];
	self.saveButton.hidden = YES;
}

#pragma mark
#pragma mark
#pragma mark LIFECYCLE
- (void)viewWillAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	self.saveButton.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.transformationsCollectionView.delegate = self;
	self.transformationsCollectionView.dataSource = self;
	[self formTransformationsArray];
	self.pictureView.image = model.picture;
	CGRect frame = [self getFrameSizeForImage:self.pictureView.image inImageView:self.pictureView];
	self.pictureView.frame = CGRectMake(self.pictureView.frame.origin.x + frame.origin.x, self.pictureView.frame.origin.y + frame.origin.y, frame.size.width, frame.size.height);
	[self setupStencil];
	[self resetStencil];
}

- (void)resetStencil
{
	self.pictureView.frame = CGRectMake(60, 60, 200, 200);
	CGRect frame = [self getFrameSizeForImage:self.pictureView.image inImageView:self.pictureView];
	self.pictureView.frame = CGRectMake(self.pictureView.frame.origin.x + frame.origin.x, self.pictureView.frame.origin.y + frame.origin.y, frame.size.width, frame.size.height);
	[self setStencilFrame:self.pinBoard.frame insideFrame:self.pictureView.frame];
	initialInsideFrame = self.pictureView.frame;
	self.pinStartX = self.pin1.center.x;
	self.pinStartY = self.pin1.center.y;
	self.pinEndX = self.pin4.center.x;
	self.pinEndY = self.pin4.center.y;
	[self setStencilFrame:self.pinBoard.frame insideFrame:self.pictureView.frame];
	self.dimensionsTextField.text = [NSString stringWithFormat:@"%i x %i", (int)self.pictureView.image.size.width, (int)self.pictureView.image.size.height];
}


- (BOOL)prefersStatusBarHidden
{
	return YES;
}



- (void)formTransformationsArray
{
	if (!self.transformationsArray) {
		self.transformationsArray = [NSMutableArray new];
	}
	for (NSString* name in @[@"Basic 2x downscale", @"Basic 2x upscale", @"SCALE2X upscale", @"SCALE3X upscale"]) {
		UILabel *label = [UILabel new];
		label.text = name;
		label.textAlignment = NSTextAlignmentCenter;
		[self.transformationsArray addObject:label];
	}
}



- (CGRect)getFrameSizeForImage:(UIImage *)image inImageView:(UIImageView *)imageView
{
    CGFloat hfactor = image.size.width / imageView.frame.size.width;
    CGFloat vfactor = image.size.height / imageView.frame.size.height;
    CGFloat factor = fmax(hfactor, vfactor);
    CGFloat newWidth = image.size.width / factor;
    CGFloat newHeight = image.size.height / factor;
    CGFloat leftOffset = (imageView.frame.size.width - newWidth) / 2;
    CGFloat topOffset = (imageView.frame.size.height - newHeight) / 2;
    return CGRectMake(leftOffset, topOffset, newWidth, newHeight);
}




- (void)setupStencil
{
	self.view1 = [UIView new];
	self.view2 = [UIView new];
	self.view3 = [UIView new];
	self.view4 = [UIView new];
	self.view1.backgroundColor = self.view2.backgroundColor = self.view3.backgroundColor = self.view4.backgroundColor = [UIColor blackColor];
	self.view1.alpha = self.view2.alpha = self.view3.alpha = self.view4.alpha = .4;
	[self.pinBoard addSubview:self.view1];
	[self.pinBoard addSubview:self.view2];
	[self.pinBoard addSubview:self.view3];
	[self.pinBoard addSubview:self.view4];
	
	self.pin1 = [UIView new];
	self.pin2 = [UIView new];
	self.pin3 = [UIView new];
	self.pin4 = [UIView new];
	self.pin1.tag = 51;
	self.pin2.tag = 52;
	self.pin3.tag = 53;
	self.pin4.tag = 54;
	self.pin1.backgroundColor = self.pin2.backgroundColor = self.pin3.backgroundColor = self.pin4.backgroundColor = [UIColor blackColor];
	self.pin1.alpha = self.pin2.alpha = self.pin3.alpha = self.pin4.alpha = .4;
	[self.pinBoard addSubview:self.pin1];
	[self.pinBoard addSubview:self.pin2];
	[self.pinBoard addSubview:self.pin3];
	[self.pinBoard addSubview:self.pin4];
}




- (void)setStencilFrame:(CGRect)frame insideFrame:(CGRect)insideFrame
{
	self.view1.frame = CGRectMake(0, 0, frame.size.width, insideFrame.origin.y);
	self.view2.frame = CGRectMake(0, insideFrame.origin.y + insideFrame.size.height, frame.size.width, frame.size.height - insideFrame.origin.y - insideFrame.size.height);
	self.view3.frame = CGRectMake(0, insideFrame.origin.y, insideFrame.origin.x, insideFrame.size.height);
	self.view4.frame = CGRectMake(insideFrame.origin.x + insideFrame.size.width, insideFrame.origin.y, frame.size.width - insideFrame.origin.x - insideFrame.size.width,  insideFrame.size.height);
	
	self.pin1.frame = CGRectMake(insideFrame.origin.x - 20, insideFrame.origin.y - 20, 40, 40);
	self.pin2.frame = CGRectMake(insideFrame.origin.x + insideFrame.size.width - 20, insideFrame.origin.y - 20, 40, 40);
	self.pin3.frame = CGRectMake(insideFrame.origin.x - 20, insideFrame.origin.y + insideFrame.size.height - 20, 40, 40);
	self.pin4.frame = CGRectMake(insideFrame.origin.x + insideFrame.size.width - 20, insideFrame.origin.y + insideFrame.size.height - 20, 40, 40);
	
	self.pin1.userInteractionEnabled = YES;
	
	CGFloat factor = self.pictureView.image.size.width / self.pictureView.frame.size.width;

	pixelsLeft = (int)((initialInsideFrame.origin.x - self.pinStartX)*factor +.5);
	pixelsRight = (int)((self.pinEndX - self.pictureView.frame.origin.x - initialInsideFrame.size.width)*factor +.5);
	pixelsTop = (int)((initialInsideFrame.origin.y - self.pinStartY)*factor +.5);
	pixelsBottom = (int)((self.pinEndY - initialInsideFrame.origin.y - initialInsideFrame.size.height)*factor +.5);
	
	self.pixelsLeftField.text = !pixelsLeft?@"": (pixelsLeft>0)? [NSString stringWithFormat:@"+%i", pixelsLeft] :[NSString stringWithFormat:@"%i", pixelsLeft];
	self.pixelsRightField.text = !pixelsRight?@"": (pixelsRight>0)? [NSString stringWithFormat:@"+%i", pixelsRight] :[NSString stringWithFormat:@"%i", pixelsRight];
	self.pixelsTopField.text = !pixelsTop?@"": (pixelsTop>0)? [NSString stringWithFormat:@"+%i", pixelsTop] :[NSString stringWithFormat:@"%i", pixelsTop];
	self.pixelsBottomField.text = !pixelsBottom?@"": (pixelsBottom>0)? [NSString stringWithFormat:@"+%i", pixelsBottom] :[NSString stringWithFormat:@"%i", pixelsBottom];
}



#pragma mark
#pragma mark
#pragma mark COLLECTION VIEW DELEGATES

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return self.transformationsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
	UILabel *label = self.transformationsArray[indexPath.row];
	label.frame = cell.bounds;
	[cell addSubview:self.transformationsArray[indexPath.row]];
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0 && ((size_t)self.pictureView.image.size.width%2 || (size_t)self.pictureView.image.size.height%2)) {
		return;
	}
	if ((indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3) && (self.pictureView.image.size.width>1000 || self.pictureView.image.size.height>1000)) {
		[self showMemoryAlert];
		return;
	}
	
	self.pictureView.image = [model.cppManager applyTransform:indexPath.row toPicture:self.pictureView.image];
	[self resetStencil];
	self.saveButton.hidden = NO;
}


#pragma mark -
#pragma mark -
#pragma mark - MAMORY ALERT
- (void)showMemoryAlert {
	UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"MEMORY ALERT" message:@"This looks more like a task for desktop. Are you sure to proceed?" delegate:self  cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alert addButtonWithTitle:@"Proceed"];
    [alert show];
}
#pragma mark -
#pragma mark -
#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
#warning testtest
	if (buttonIndex == 1) {
		self.pictureView.image = [model.cppManager applyTransform:0 toPicture:self.pictureView.image];
		[self resetStencil];
		self.saveButton.hidden = NO;
	}
}



#pragma mark
#pragma mark
#pragma mark ACTIONS
- (IBAction)backToPreview:(id)sender
{
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)rotateButtonPressed:(id)sender
{
	self.pictureView.image = [model.cppManager rotatePicture:self.pictureView.image];
	[self resetStencil];
	self.saveButton.hidden = NO;
}
- (IBAction)flipButtonPressed:(id)sender
{
	self.pictureView.image = [model.cppManager flipPicture:self.pictureView.image];
	self.saveButton.hidden = NO;
}

- (IBAction)cropButtonPressed:(UIButton *)sender
{
	self.pictureView.image = [model.cppManager cropAddPixelsToPicture:self.pictureView.image left:pixelsLeft right:pixelsRight top:pixelsTop bottom:pixelsBottom];
	[self resetStencil];
	self.saveButton.hidden = NO;
}

- (IBAction)saveButtonPressed:(UIButton *)sender
{
	[model saveImage:self.pictureView.image toFile:model.fileNameFullPath];
	model.picture = self.pictureView.image;
	self.saveButton.hidden = YES;
}



#pragma mark -
#pragma mark -
#pragma mark - touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint point;
	for (UITouch *touch in [touches allObjects]) {
		point = [touch locationInView:self.pinBoard];
		UIView *view = [self.pinBoard hitTest:point withEvent:event];
		switch (view.tag) {
			case 51:
				self.pinStartX = point.x;
				self.pinStartY = point.y;
//				self.pin1.center = point;
				[self setStencilFrame:self.pinBoard.frame insideFrame:CGRectMake(self.pinStartX, self.pinStartY, self.pinEndX-self.pinStartX, self.pinEndY-self.pinStartY)];
				
				break;
			case 52:
				self.pinEndX = point.x;
				self.pinStartY = point.y;
				[self setStencilFrame:self.pinBoard.frame insideFrame:CGRectMake(self.pinStartX, self.pinStartY, self.pinEndX-self.pinStartX, self.pinEndY-self.pinStartY)];
//				self.pin2.center = point;
				break;
			case 53:
				self.pinStartX = point.x;
				self.pinEndY = point.y;
				[self setStencilFrame:self.pinBoard.frame insideFrame:CGRectMake(self.pinStartX, self.pinStartY, self.pinEndX-self.pinStartX, self.pinEndY-self.pinStartY)];
//				self.pin3.center = point;
				break;
			case 54:
				self.pinEndX = point.x;
				self.pinEndY = point.y;
				[self setStencilFrame:self.pinBoard.frame insideFrame:CGRectMake(self.pinStartX, self.pinStartY, self.pinEndX-self.pinStartX, self.pinEndY-self.pinStartY)];
//				self.pin4.center = point;
				break;
			
			default:
				break;
		}
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesMoved:touches withEvent:event];
}


@end
