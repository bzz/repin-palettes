//
//  ProjectManagerViewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 09/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectCell.h"

@interface ProjectManagerViewController : UIViewController
<UICollectionViewDataSource,
UICollectionViewDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate
>


@end
