//
//  PaletteCell.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 19/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaletteCell : UICollectionViewCell
@property (strong, nonatomic) UIColor *color;


@end
