//
//  UIView+ColorAtPoint.h
//  SpaceKit
//
//  Created by Alex Buzov on 11/13/2013.
//  Copyright (c) 2013 SpaceNation Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView (ColorAtPoint)

- (UIColor *)colorAtPoint:(CGPoint)point;

@end
