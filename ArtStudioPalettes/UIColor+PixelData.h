//
//  UIColor+PixelData.h
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/27/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PixelData)

+ (UIColor *)colorFromPixelColorAtX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image;
- (UIColor*)inverseColor;
+ (UIColor*)hex:(NSInteger)hex;

@end
