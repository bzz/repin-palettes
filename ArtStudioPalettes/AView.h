//
//  AView.h
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/19/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"

@interface AView : UIView
@property (nonatomic, strong) UIImage *image;

@end
