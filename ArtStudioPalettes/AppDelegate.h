//
//  AppDelegate.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 09/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Picture.h"
#import "Palette.h"
#import "Color.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
		   
		   
@end
