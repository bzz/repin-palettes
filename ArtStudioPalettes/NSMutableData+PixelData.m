//
//  NSMutableData+PixelData.m
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/25/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "NSMutableData+PixelData.h"


@implementation NSMutableData (PixelData)

///Converts image pixels to mutable data
+ (NSMutableData *)dataFromImage:(UIImage *)image
{
	if (image.size.width && image.size.height) {
	CGImageRef imageRef = [image CGImage];
	NSUInteger width = CGImageGetWidth(imageRef);
	NSUInteger height = CGImageGetHeight(imageRef);
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	NSMutableData *data = [NSMutableData dataWithLength:height * width * 4];
	unsigned char *rawData = [data mutableBytes];
	CGContextRef context = CGBitmapContextCreate(rawData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
	CGColorSpaceRelease(colorSpace);
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
	CGContextRelease(context);
	data = [NSMutableData dataWithBytes:(const void *)rawData length:sizeof(unsigned char)*(height * width * 4)];
	return data;
	} else return nil;
}

+ (NSMutableData *)dataChangePixelColorRed:(int)red Green:(int)green Blue:(int)blue Alpha:(int)alpha forX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image {
		unsigned char *rawData = [data mutableBytes];
		CGImageRef imageRef = [image CGImage];
		NSUInteger width = CGImageGetWidth(imageRef);
		NSUInteger height = CGImageGetHeight(imageRef);

	if (x>=0 && y>=0 && x<height && y<width) {
		
		rawData[ (x + width * y) * 4    ] = (unsigned char)red;
		rawData[ (x + width * y) * 4 +1 ] = (unsigned char)green;
		rawData[ (x + width * y) * 4 +2 ] = (unsigned char)blue;
		rawData[ (x + width * y) * 4 +3 ] = (unsigned char)alpha;
		
		data = [NSMutableData dataWithBytes:(const void *)rawData length:sizeof(unsigned char)*(height * width * 4)];
	}
	
	return data;
}



+ (NSMutableData *)dataApplyRed:(CGFloat)red Green:(CGFloat)green Blue:(CGFloat)blue Alpha:(CGFloat)alpha toData:(NSMutableData *)data forImage:(UIImage *)image {
	
	if (data && image) {

		unsigned char *rawData = [data mutableBytes];
		CGImageRef imageRef = [image CGImage];
		NSUInteger width = CGImageGetWidth(imageRef);
		NSUInteger height = CGImageGetHeight(imageRef);
	
		for (int ii = 0; ii < width*height*4; ii+=4) {
			rawData[ii]   += red;
			rawData[ii+1] += green;
			rawData[ii+2] += blue;
			rawData[ii+3] += alpha;
		}
	
		data = [NSMutableData dataWithBytes:(const void *)rawData length:sizeof(unsigned char)*(height * width * 4)];
	}
	return data;
}



+ (NSMutableData *)dataFillWithColorRed:(int)red Green:(int)green Blue:(int)blue Alpha:(int)alpha fromX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image {
	
	unsigned char *rawData = [data mutableBytes];
	CGImageRef imageRef = [image CGImage];
	NSUInteger width = CGImageGetWidth(imageRef);
	NSUInteger height = CGImageGetHeight(imageRef);
	
	unsigned char redC;
	unsigned char greenC;
	unsigned char blueC;
	unsigned char alphaC;
	
	int xCoord = x;
	int yCoord = y;
	
	redC   = rawData[ (xCoord + width * yCoord) * 4    ];
	greenC = rawData[ (xCoord + width * yCoord) * 4 +1 ];
	blueC  = rawData[ (xCoord + width * yCoord) * 4 +2 ];
	alphaC = rawData[ (xCoord + width * yCoord) * 4 +3 ];

	
	if (!((red==redC) && (green==greenC) && (blue==blueC) && (alpha==alphaC))) {
		NSMutableArray *pointsToFill = [[NSMutableArray alloc]init];
	
		[pointsToFill addObject:[NSNumber numberWithInt:xCoord]];
		[pointsToFill addObject:[NSNumber numberWithInt:yCoord]];

		while ([pointsToFill count]) {
			yCoord = [[pointsToFill lastObject]intValue];
			[pointsToFill removeLastObject];
			xCoord = [[pointsToFill lastObject]intValue];
			[pointsToFill removeLastObject];
		
		
			//IF POINT_IS_SAME_COLOR
			if ((rawData[(xCoord+width*yCoord)*4]==redC) && (rawData[(xCoord+width*yCoord)*4+1]==greenC) && (rawData[(xCoord+width*yCoord)*4+2]==blueC) && (rawData[(xCoord+width*yCoord)*4+3]==alphaC)) {

				//POINT_SET_COLOR
				rawData[(xCoord+width*yCoord)*4]=(unsigned char)red; rawData[(xCoord+width*yCoord)*4+1]=(unsigned char)green; rawData[(xCoord+width*yCoord)*4+2]=(unsigned char)blue; rawData[(xCoord+width*yCoord)*4+3] = (unsigned char)alpha;

			
				if ((xCoord >= 1 && xCoord < width +1) &&
					(yCoord >= 0 && yCoord < height) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord-1]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord  ]];
				}
				
				if ((xCoord >= -1 && xCoord < width -1) &&
					(yCoord >= 0 && yCoord < height) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord+1]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord  ]];
				}
				
				if ((xCoord >= 0 && xCoord < width) &&
					(yCoord >= 1 && yCoord < height +1) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord  ]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord-1]];
				}
				
				if ((xCoord >= 0 && xCoord < width) &&
					(yCoord >= -1 && yCoord < height -1) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord  ]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord+1]];
				}
			}
		}
		pointsToFill = nil;
	
		data = [NSMutableData dataWithBytes:(const void *)rawData length:sizeof(unsigned char)*(height * width * 4)];
	
	}
	return data;
	
}

+ (NSMutableData *)dataContourFillWithColorRed:(int)red Green:(int)green Blue:(int)blue Alpha:(int)alpha fromX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image {
	
	unsigned char *rawData = [data mutableBytes];
	CGImageRef imageRef = [image CGImage];
	NSUInteger width = CGImageGetWidth(imageRef);
	NSUInteger height = CGImageGetHeight(imageRef);
	
	unsigned char redC;
	unsigned char greenC;
	unsigned char blueC;
	unsigned char alphaC;
	
	int xCoord = x;
	int yCoord = y;
	
	redC   = rawData[ (xCoord + width * yCoord) * 4    ];
	greenC = rawData[ (xCoord + width * yCoord) * 4 +1 ];
	blueC  = rawData[ (xCoord + width * yCoord) * 4 +2 ];
	alphaC = rawData[ (xCoord + width * yCoord) * 4 +3 ];
	
	
	if (!((red==redC) && (green==greenC) && (blue==blueC) && (alpha==alphaC))) {
		NSMutableArray *pointsToFill = [[NSMutableArray alloc]init];
		
		[pointsToFill addObject:[NSNumber numberWithInt:xCoord]];
		[pointsToFill addObject:[NSNumber numberWithInt:yCoord]];
		
		while ([pointsToFill count]) {
			yCoord = [[pointsToFill lastObject]intValue];
			[pointsToFill removeLastObject];
			xCoord = [[pointsToFill lastObject]intValue];
			[pointsToFill removeLastObject];
			
			
			//IF POINT_IS_SAME_COLOR
			if ((rawData[(xCoord+width*yCoord)*4]==redC) && (rawData[(xCoord+width*yCoord)*4+1]==greenC) && (rawData[(xCoord+width*yCoord)*4+2]==blueC) && (rawData[(xCoord+width*yCoord)*4+3]==alphaC)) {
				
				//POINT_SET_COLOR
				rawData[(xCoord+width*yCoord)*4]=(unsigned char)red; rawData[(xCoord+width*yCoord)*4+1]=(unsigned char)green; rawData[(xCoord+width*yCoord)*4+2]=(unsigned char)blue; rawData[(xCoord+width*yCoord)*4+3] = (unsigned char)alpha;
				
				
				if ((xCoord >= 1 && xCoord < width +1) &&
					(yCoord >= 0 && yCoord < height) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord-1]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord  ]];
				}
				
				if ((xCoord >= -1 && xCoord < width -1) &&
					(yCoord >= 0 && yCoord < height) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord+1]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord  ]];
				}
				
				if ((xCoord >= 0 && xCoord < width) &&
					(yCoord >= 1 && yCoord < height +1) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord  ]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord-1]];
				}
				
				if ((xCoord >= 0 && xCoord < width) &&
					(yCoord >= -1 && yCoord < height -1) ) {
					[pointsToFill addObject:[NSNumber numberWithInt:xCoord  ]];
					[pointsToFill addObject:[NSNumber numberWithInt:yCoord+1]];
				}
			}

			
		}
		pointsToFill = nil;
		
		data = [NSMutableData dataWithBytes:(const void *)rawData length:sizeof(unsigned char)*(height * width * 4)];
		
	}
	return data;
	
}



+ (NSMutableData *)blackWhiteSketchFromImage:(UIImage*)image {
		
	NSMutableData *data = [NSMutableData dataFromImage:image];
	
	unsigned char *rawData = [data mutableBytes];
	CGImageRef imageRef = [image CGImage];
	NSUInteger width = CGImageGetWidth(imageRef);
	NSUInteger height = CGImageGetHeight(imageRef);
	
	for (int y=0; y<height; y++) {
		for (int x=0; x<width; x++) {
			if (rawData[ (x + width * y) * 4    ] < 123 &&
				rawData[ (x + width * y) * 4 +1 ] < 123 &&
				rawData[ (x + width * y) * 4 +2 ] < 123 &&
				rawData[ (x + width * y) * 4 +3 ] > 123) {
				
				rawData[ (x + width * y) * 4    ] = 0;
				rawData[ (x + width * y) * 4 +1 ] = 0;
				rawData[ (x + width * y) * 4 +2 ] = 0;
				rawData[ (x + width * y) * 4 +3 ] = 255;
			} else {
				rawData[ (x + width * y) * 4    ] = 255;
				rawData[ (x + width * y) * 4 +1 ] = 255;
				rawData[ (x + width * y) * 4 +2 ] = 255;
				rawData[ (x + width * y) * 4 +3 ] = 255;
			}
		}
	}
	
	data = [NSMutableData dataWithBytes:(const void *)rawData length:sizeof(unsigned char)*(height * width * 4)];
	
	return data;
}






+ (NSMutableData *)dataApplyScaleInterpolation2x:(NSMutableData *)data destinationImage:(UIImage *)image {
	if (data) {
		unsigned char *rawData = [data mutableBytes];
		
		CGImageRef imageRef = [image CGImage];
		int width = (int)CGImageGetWidth(imageRef);
		int height = (int)CGImageGetHeight(imageRef) ;
		unsigned char rawData2[height * width * 4];
		
		int ii = 0;
		for (int hh = 0; hh < height; hh+=2) {
			for (int ww = 0; ww < width; ww+=2) {
			//1
				rawData2[ (ww + width * hh) * 4    ] = rawData[ii  ];
				rawData2[ (ww + width * hh) * 4 +1 ] = rawData[ii+1];
				rawData2[ (ww + width * hh) * 4 +2 ] = rawData[ii+2];
				rawData2[ (ww + width * hh) * 4 +3 ] = rawData[ii+3];
			//2
				rawData2[ (ww + width * hh) * 4 +4 ] = rawData[ii  ];
				rawData2[ (ww + width * hh) * 4 +5 ] = rawData[ii+1];
				rawData2[ (ww + width * hh) * 4 +6 ] = rawData[ii+2];
				rawData2[ (ww + width * hh) * 4 +7 ] = rawData[ii+3];
				
			//3
				rawData2[ (ww + width * (hh+1)) * 4    ] = rawData[ii  ];
				rawData2[ (ww + width * (hh+1)) * 4 +1 ] = rawData[ii+1];
				rawData2[ (ww + width * (hh+1)) * 4 +2 ] = rawData[ii+2];
				rawData2[ (ww + width * (hh+1)) * 4 +3 ] = rawData[ii+3];
			//4
				rawData2[ (ww + width * (hh+1)) * 4 +4 ] = rawData[ii  ];
				rawData2[ (ww + width * (hh+1)) * 4 +5 ] = rawData[ii+1];
				rawData2[ (ww + width * (hh+1)) * 4 +6 ] = rawData[ii+2];
				rawData2[ (ww + width * (hh+1)) * 4 +7 ] = rawData[ii+3];
				ii+=4;
			}
		}
		NSMutableData *data2 = [NSMutableData dataWithBytes:(const void *)rawData2 length:sizeof(unsigned char)*(height * width * 4)];
		return data2;
	} else return nil;
}


+ (NSMutableData *)dataApplyEPXScale2x:(NSMutableData *)data destinationImage:(UIImage *)image {
/*
   A    --\ 1 2
 C P B  --/ 3 4
   D
 
 1=P; 2=P; 3=P; 4=P;
 IF C==A AND C!=D AND A!=B => 1=A
 IF A==B AND A!=C AND B!=D => 2=B
 IF B==D AND B!=A AND D!=C => 4=D
 IF D==C AND D!=B AND C!=A => 3=C

*/
	if (data) {
		unsigned char *rawData = [data mutableBytes];
		CGImageRef imageRef = [image CGImage];
		int width = (int)CGImageGetWidth(imageRef);
		int height = (int)CGImageGetHeight(imageRef) ;

		unsigned char rawData0[sizeof(unsigned char)*(height * width * 4 /4 *5)];
		int rr = 0;
		for (int hh = 0; hh < height/2; hh++) {
			for (int ww = 0; ww < width/2; ww++) {
			//P
				rawData0[rr  ]=  rawData[ (ww + width/2 * hh) * 4    ];
				rawData0[rr+1]=  rawData[ (ww + width/2 * hh) * 4 +1 ];
				rawData0[rr+2]=  rawData[ (ww + width/2 * hh) * 4 +2 ];
				rawData0[rr+3]=  rawData[ (ww + width/2 * hh) * 4 +3 ];
			//A
				if (hh==0) {
					rawData0[rr+4]=255;
					rawData0[rr+5]=255;
					rawData0[rr+6]=255;
					rawData0[rr+7]=0;
				} else {
					rawData0[rr+4]=rawData[ (ww + width/2 *(hh-1)) * 4    ];
					rawData0[rr+5]=rawData[ (ww + width/2 *(hh-1)) * 4 +1 ];
					rawData0[rr+6]=rawData[ (ww + width/2 *(hh-1)) * 4 +2 ];
					rawData0[rr+7]=rawData[ (ww + width/2 *(hh-1)) * 4 +3 ];
				}
				
			//B
				if (ww==width/2) {
					rawData0[rr+4]=255;
					rawData0[rr+5]=255;
					rawData0[rr+6]=255;
					rawData0[rr+7]=0;
				} else {
				rawData0[rr+8 ]=rawData[ (ww +1 + width/2 * hh) * 4    ];
				rawData0[rr+9 ]=rawData[ (ww +1 + width/2 * hh) * 4 +1 ];
				rawData0[rr+10]=rawData[ (ww +1 + width/2 * hh) * 4 +2 ];
				rawData0[rr+11]=rawData[ (ww +1 + width/2 * hh) * 4 +3 ];
				}
			//C
				if (ww==0) {
					rawData0[rr+4]=255;
					rawData0[rr+5]=255;
					rawData0[rr+6]=255;
					rawData0[rr+7]=0;
				} else {
				rawData0[rr+12]=rawData[ (ww -1 + width/2 * hh) * 4    ];
				rawData0[rr+13]=rawData[ (ww -1 + width/2 * hh) * 4 +1 ];
				rawData0[rr+14]=rawData[ (ww -1 + width/2 * hh) * 4 +2 ];
				rawData0[rr+15]=rawData[ (ww -1 + width/2 * hh) * 4 +3 ];
				}
			//D
				if (hh==height/2) {
					rawData0[rr+4]=255;
					rawData0[rr+5]=255;
					rawData0[rr+6]=255;
					rawData0[rr+7]=0;
				} else {
				rawData0[rr+16]=rawData[ (ww + width/2 *(hh+1)) * 4    ];
				rawData0[rr+17]=rawData[ (ww + width/2 *(hh+1)) * 4 +1 ];
				rawData0[rr+18]=rawData[ (ww + width/2 *(hh+1)) * 4 +2 ];
				rawData0[rr+19]=rawData[ (ww + width/2 *(hh+1)) * 4 +3 ];
				}
				
				rr+=5*4;
			}
		}

		unsigned char rawData2[height * width * 4];
		
		int ii = 0;

		
		
		for (int hh = 0; hh < height; hh+=2) {
			for (int ww = 0; ww < width; ww+=2) {
			//1=P
				rawData2[ (ww + width * hh) * 4    ] = rawData0[ii];
				rawData2[ (ww + width * hh) * 4 +1 ] = rawData0[ii+1];
				rawData2[ (ww + width * hh) * 4 +2 ] = rawData0[ii+2];
				rawData2[ (ww + width * hh) * 4 +3 ] = rawData0[ii+3];
			//2=P
				rawData2[ (ww + width * hh) * 4 +4 ] = rawData0[ii];
				rawData2[ (ww + width * hh) * 4 +5 ] = rawData0[ii+1];
				rawData2[ (ww + width * hh) * 4 +6 ] = rawData0[ii+2];
				rawData2[ (ww + width * hh) * 4 +7 ] = rawData0[ii+3];
			//3=P
				rawData2[ (ww + width * (hh+1)) * 4    ] = rawData0[ii];
				rawData2[ (ww + width * (hh+1)) * 4 +1 ] = rawData0[ii+1];
				rawData2[ (ww + width * (hh+1)) * 4 +2 ] = rawData0[ii+2];
				rawData2[ (ww + width * (hh+1)) * 4 +3 ] = rawData0[ii+3];
			//4=P
				rawData2[ (ww + width * (hh+1)) * 4 +4 ] = rawData0[ii];
				rawData2[ (ww + width * (hh+1)) * 4 +5 ] = rawData0[ii+1];
				rawData2[ (ww + width * (hh+1)) * 4 +6 ] = rawData0[ii+2];
				rawData2[ (ww + width * (hh+1)) * 4 +7 ] = rawData0[ii+3];
				
				


				//C==A?
				if ((rawData0[ii+12] == rawData0[ii+4]) &&
					(rawData0[ii+13] == rawData0[ii+5]) &&
					(rawData0[ii+14] == rawData0[ii+6]) &&
					(rawData0[ii+15] == rawData0[ii+7])) {
					
					//D!=C?
					if (!((rawData0[ii+16] == rawData0[ii+12])  &&
						(rawData0[ii+17] == rawData0[ii+13])  &&
						(rawData0[ii+18] == rawData0[ii+14])  &&
						(rawData0[ii+19] == rawData0[ii+15]))) {
						
						//A!=B?
						if (!((rawData0[ii+4] == rawData0[ii+8])  &&
							(rawData0[ii+5] == rawData0[ii+9])  &&
							(rawData0[ii+6] == rawData0[ii+10]) &&
							(rawData0[ii+7] == rawData0[ii+11]))) {
				//1=A
					rawData2[ (ww + width * hh) * 4    ] = rawData0[ii+12];
					rawData2[ (ww + width * hh) * 4 +1 ] = rawData0[ii+13];
					rawData2[ (ww + width * hh) * 4 +2 ] = rawData0[ii+14];
					rawData2[ (ww + width * hh) * 4 +3 ] = rawData0[ii+15];
						}
					}
				}


				
				//A==B?
				if ((rawData0[ii+4] == rawData0[ii+8])  &&
					(rawData0[ii+5] == rawData0[ii+9])  &&
					(rawData0[ii+6] == rawData0[ii+10]) &&
					(rawData0[ii+7] == rawData0[ii+11])) {
					//C!=A?
					if (!((rawData0[ii+12] == rawData0[ii+4]) &&
						   (rawData0[ii+13] == rawData0[ii+5]) &&
						   (rawData0[ii+14] == rawData0[ii+6]) &&
						   (rawData0[ii+15] == rawData0[ii+7]))) {
						
						//B!=D?
						if (!((rawData0[ii+8] ==  rawData0[ii+16])  &&
							  (rawData0[ii+9] ==  rawData0[ii+17])  &&
							  (rawData0[ii+10] == rawData0[ii+18])  &&
							  (rawData0[ii+11] == rawData0[ii+19]))) {
				//2=B
					rawData2[ (ww + width * hh) * 4 +4 ] = rawData0[ii+8];
					rawData2[ (ww + width * hh) * 4 +5 ] = rawData0[ii+9];
					rawData2[ (ww + width * hh) * 4 +6 ] = rawData0[ii+10];
					rawData2[ (ww + width * hh) * 4 +7 ] = rawData0[ii+11];
						}
					}
				}

				
				//a4 b8 c12 d16


				//IF B==D AND B!=A AND D!=C => 4=D
				//B==D?
				if ((rawData0[ii+8] ==  rawData0[ii+16])  &&
					(rawData0[ii+9] ==  rawData0[ii+17])  &&
					(rawData0[ii+10] == rawData0[ii+18])  &&
					(rawData0[ii+11] == rawData0[ii+19])) {
					//D!=C?
					if (!((rawData0[ii+16] == rawData0[ii+12])  &&
						  (rawData0[ii+17] == rawData0[ii+13])  &&
						  (rawData0[ii+18] == rawData0[ii+14])  &&
						  (rawData0[ii+19] == rawData0[ii+15]))) {
						
						//A!=B?
						if (!((rawData0[ii+4] == rawData0[ii+8])  &&
							  (rawData0[ii+5] == rawData0[ii+9])  &&
							  (rawData0[ii+6] == rawData0[ii+10]) &&
							  (rawData0[ii+7] == rawData0[ii+11]))) {
							//4=D
							rawData2[ (ww + width * (hh+1)) * 4 +4 ] = rawData0[ii+16];
							rawData2[ (ww + width * (hh+1)) * 4 +5 ] = rawData0[ii+17];
							rawData2[ (ww + width * (hh+1)) * 4 +6 ] = rawData0[ii+18];
							rawData2[ (ww + width * (hh+1)) * 4 +7 ] = rawData0[ii+19];
						}
					}
				}
				
				
				 
				//IF D==C AND D!=B AND C!=A => 3=C
				//D==C?
				if ((rawData0[ii+16] == rawData0[ii+12])  &&
					(rawData0[ii+17] == rawData0[ii+13])  &&
					(rawData0[ii+18] == rawData0[ii+14])  &&
					(rawData0[ii+19] == rawData0[ii+15])) {
					//C!=A?
					if (!((rawData0[ii+12] == rawData0[ii+4]) &&
						  (rawData0[ii+13] == rawData0[ii+5]) &&
						  (rawData0[ii+14] == rawData0[ii+6]) &&
						  (rawData0[ii+15] == rawData0[ii+7]))) {
						
						//B!=D?
						if (!((rawData0[ii+8] ==  rawData0[ii+16])  &&
							  (rawData0[ii+9] ==  rawData0[ii+17])  &&
							  (rawData0[ii+10] == rawData0[ii+18])  &&
							  (rawData0[ii+11] == rawData0[ii+19]))) {
							//3=C
							rawData2[ (ww + width * (hh+1)) * 4    ] = rawData0[ii+12];
							rawData2[ (ww + width * (hh+1)) * 4 +1 ] = rawData0[ii+13];
							rawData2[ (ww + width * (hh+1)) * 4 +2 ] = rawData0[ii+14];
							rawData2[ (ww + width * (hh+1)) * 4 +3 ] = rawData0[ii+15];
						}
					}
				}

	 ii+=5*4;

	 
	 }
		}
		
		
		
		
		
		NSMutableData *data2 = [NSMutableData dataWithBytes:(const void *)rawData2 length:sizeof(unsigned char)*(height * width * 4)];
		return data2;
	} else return nil;
}

+ (NSMutableData *)dataPaletteDataFromImage:(UIImage *)image {
	NSMutableData *data;
	if (image) {
//		unsigned char *rawData = [data mutableBytes];
		CGImageRef imageRef = image.CGImage;

		size_t width = CGImageGetWidth(imageRef);
		size_t height = CGImageGetHeight(imageRef);
		
		unsigned char rawData[sizeof(unsigned char)*(height * width * 4)];
		
//		for (int ii = 0; ii < width*height*4; ii+=4) {
//			rawData[ii]   += red;
//			rawData[ii+1] += green;
//			rawData[ii+2] += blue;
//			rawData[ii+3] += alpha;
//		}
		data = [NSMutableData dataWithBytes:(const void *)rawData length:sizeof(unsigned char)*(height * width * 4)];
	}
	return data;
}










@end
