//
//  FileCell.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 11/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FileCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UITextField *fileNameField;
@property (weak, nonatomic) IBOutlet UIImageView *pictureView;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *makeCopyButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;



@end
