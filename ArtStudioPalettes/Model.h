//
//  PictureModel.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 26/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPPManager.h"
#import "Picture.h"
#import "Palette.h"
#import "Color.h"


@interface Model : NSObject

@property (nonatomic, strong) CPPManager *cppManager;
@property (nonatomic, strong) UIImage *picture;
@property (nonatomic, strong) UIImage *pictureArea;
@property (nonatomic, strong) NSMutableData *pictureAreaData;
@property (nonatomic, strong) NSMutableData *pictureAreaDataUndoBuffer;
@property (nonatomic) CGRect pictureAreaRect;


@property (nonatomic, strong) NSMutableData *data;

@property (nonatomic, strong) NSString* fileNameFullPath;
@property (nonatomic, strong) NSMutableArray* paletteColorsArray;
@property (nonatomic, strong) NSMutableArray* paletteUserColorsArray;
@property (nonatomic, strong) UIImage* paletteImage;

@property (nonatomic, strong) UIColor *selectedColor;
@property (nonatomic) int selectedColorRed;
@property (nonatomic) int selectedColorGreen;
@property (nonatomic) int selectedColorBlue;
@property (nonatomic) int selectedColorAlpha;



@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSFileManager *fileManager;
//@property (nonatomic) dispatch_queue_t Q;



+ (instancetype)sharedInstance;


- (void)formPaletteColorsArray;
- (void)formPaletteUserColorsArrayFromImage:(UIImage*)image;
- (BOOL)colorExistsInUserPalette:(UIColor*)color;
- (BOOL)colorExistsInPaletteColorsArray:(UIColor*)color;
- (void)saveImage:(UIImage*)image toFile:(NSString *)fileName;


@end
