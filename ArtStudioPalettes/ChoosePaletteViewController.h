//
//  ChoosePaletteViewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 27/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"


@interface ChoosePaletteViewController : UIViewController
<UICollectionViewDataSource,
UICollectionViewDelegate>


@end
