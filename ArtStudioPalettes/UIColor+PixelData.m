//
//  UIColor+PixelData.m
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/27/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "UIColor+PixelData.h"

@implementation UIColor (PixelData)

+ (UIColor *)colorFromPixelColorAtX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image {
	unsigned char *rawData = [data mutableBytes];
	CGImageRef imageRef = [image CGImage];
	NSUInteger width = CGImageGetWidth(imageRef);
	CGFloat red   = ((CGFloat) rawData[ (x + width * y) * 4    ]) /255.0f;
	CGFloat green = ((CGFloat) rawData[ (x + width * y) * 4 +1 ]) /255.0f;
	CGFloat blue  = ((CGFloat) rawData[ (x + width * y) * 4 +2 ]) /255.0f;
	CGFloat alpha = ((CGFloat) rawData[ (x + width * y) * 4 +3 ]) /255.0f;
	UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
	
	return color;
}

- (UIColor*)inverseColor
{
    CGFloat r,g,b,a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return [UIColor colorWithRed:1.-r green:1.-g blue:1.-b alpha:a];
}

+ (UIColor*)hex:(NSInteger)hex
{
    return [UIColor colorWithRed:((hex & 0xFF0000) >> 16)/255.0 green:((hex & 0xFF00) >> 8)/255.0 blue:(hex & 0xFF)/255.0 alpha:1.0];
}

@end
