//
//  EditorViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 12/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//


enum {COLLECTION_PALETTE=101, COLLECTION_TOOL=102};



#import "EditorViewController.h"
#import "PreviewController.h"
#import "PaletteCell.h"
#import "PaletteViewController.h"


@interface EditorViewController ()
{
	Model *model;
	CGPoint joystickPosition;
	CGPoint joystickOffset;
	BOOL joystickOn;
	
}
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation EditorViewController


#pragma mark -
#pragma mark -
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit
{
	joystickPosition = CGPointZero;
	joystickOffset = CGPointZero;
	joystickOn = NO;
	model = [Model sharedInstance];
	self.artView = [AView new];
	self.saveButton.hidden = YES;
}

#pragma mark -
#pragma mark -
#pragma mark - LIFECYCLE
- (void)viewWillAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:NO animated:YES];
    self.saveButton.hidden = YES;
    self.selectedColor = model.selectedColor;
}
- (void)viewDidAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	[self.paletteCollectionView reloadData];
}
- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.colorView.backgroundColor = model.selectedColor;
	self.toolsCollectionView.delegate = self;
	self.toolsCollectionView.dataSource = self;
	self.paletteCollectionView.delegate = self;
	self.paletteCollectionView.dataSource = self;
	
	[self formToolsArray];
	_cellSize = 20;
	self.gridX = _cellSize;
	self.gridY = _cellSize;
	
	_insideX = model.pictureArea.size.width/2-(int)self.gridView.bounds.size.width/_cellSize/2;
	_insideY = model.pictureArea.size.height/2-(int)self.gridView.bounds.size.height/_cellSize/2;
	
	self.artView.image = model.pictureArea;
	model.pictureAreaData = [NSMutableData dataFromImage:self.artView.image];
	model.pictureAreaDataUndoBuffer = model.pictureAreaData;
	
	self.artView.frame = CGRectMake((self.artViewFrameView.bounds.size.width - model.pictureArea.size.width)/2, (self.artViewFrameView.bounds.size.height - model.pictureArea .size.height)/2, model.pictureArea .size.width, model.pictureArea.size.height);
	self.artView.backgroundColor = [UIColor lightGrayColor];
	[self.artViewFrameView addSubview:self.artView];
	
	[self.gridView layoutIfNeeded];
	[self populateViewWithCells];
	
	
	
	[self.zoomLevelButton1 addTarget:self action:@selector(zoomLevelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[self.zoomLevelButton2 addTarget:self action:@selector(zoomLevelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[self.zoomLevelButton3 addTarget:self action:@selector(zoomLevelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[self.zoomLevelButton4 addTarget:self action:@selector(zoomLevelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[self.zoomLevelButton5 addTarget:self action:@selector(zoomLevelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[self.zoomLevelButton1 setTitle:[NSString stringWithFormat:@"x%i", (int)self.zoomLevelButton1.tag] forState:UIControlStateNormal];
	[self.zoomLevelButton2 setTitle:[NSString stringWithFormat:@"x%i", (int)self.zoomLevelButton2.tag] forState:UIControlStateNormal];
	[self.zoomLevelButton3 setTitle:[NSString stringWithFormat:@"x%i", (int)self.zoomLevelButton3.tag] forState:UIControlStateNormal];
	[self.zoomLevelButton4 setTitle:[NSString stringWithFormat:@"x%i", (int)self.zoomLevelButton4.tag] forState:UIControlStateNormal];
	[self.zoomLevelButton5 setTitle:[NSString stringWithFormat:@"x%i", (int)self.zoomLevelButton5.tag] forState:UIControlStateNormal];
	
	
}


- (void)formToolsArray
{
	if (!self.toolsArray) {
		self.toolsArray = [NSMutableArray new];
	}
	for (NSString* fileName in @[@"pen1", @"fill", @"pen_thick", @"undo", @"picker", @"eraser", @"magnify_plus", @"qmoon"]) {
		UIImage *image = [UIImage imageNamed:[fileName stringByAppendingPathExtension:@"png"]];
		if (image) {
			UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
			imageView.backgroundColor = [UIColor cyanColor];
			[self.toolsArray addObject:imageView];
		}
	}
}



- (void)populateViewWithCells
{
	CGFloat xCoord = 0;
	CGFloat yCoord = 0;
	for (UIView *cell in self.gridView.subviews) {
		if ([cell isKindOfClass:[UIView class]]) {
			[cell removeFromSuperview];
		}
	}
	for (NSInteger y = 0; y < self.gridView.bounds.size.height; y = y + _cellSize) {
		for (NSInteger x = 0; x < self.gridView.bounds.size.width; x = x + _cellSize) {
			if (xCoord+_insideX < 0 || yCoord+_insideY < 0) {
			} else if (xCoord+_insideX >= model.pictureArea.size.width || yCoord+_insideY >= model.pictureArea.size.height) {
			} else {
				UIView *cellView = [[UIView alloc]initWithFrame:CGRectMake(x, y, _cellSize, _cellSize)];
				cellView.layer.borderColor = [UIColor grayColor].CGColor;
				cellView.layer.borderWidth = .5;
				cellView.tag = 99;

				cellView.backgroundColor = [UIColor colorFromPixelColorAtX:xCoord+_insideX Y:yCoord+_insideY ofData:model.pictureAreaData forImage:self.artView.image];
				[self.gridView addSubview:cellView];
				
				if (_pickMode || _fillMode) {
					cellView.layer.borderWidth = 0;
				}
			}
			xCoord++;
		}
		self.xCount = (CGFloat)xCoord;
		xCoord = 0;
		yCoord++;
		self.yCount = (CGFloat)yCoord;
	}
	self.zoomLevelButtonsView.hidden = YES;
	[self.gridView addSubview:self.zoomLevelButtonsView];
}



#pragma mark -
#pragma mark -
#pragma mark COLLECTION VIEW DELEGATES

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	switch (collectionView.tag) {
		case COLLECTION_PALETTE:
			return 1;
			break;
		case COLLECTION_TOOL:
			return 1;
			break;
		default:
			return 0;
			break;
	}
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	switch (collectionView.tag) {
		case COLLECTION_PALETTE: {
//			return section? model.paletteUserColorsArray.count: model.paletteColorsArray.count;
			return model.paletteUserColorsArray.count;
		}
			break;
		case COLLECTION_TOOL: {
			return self.toolsArray.count;
		}
			break;
		default: {return 0;}
			break;
	}
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	switch (collectionView.tag) {
		case COLLECTION_PALETTE: {
			PaletteCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"paletteCell" forIndexPath:indexPath];
			cell.color = model.paletteUserColorsArray[indexPath.row];
			return cell;
		}
			break;
		case COLLECTION_TOOL: {
			UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"toolCell" forIndexPath:indexPath];
			[cell addSubview:self.toolsArray[indexPath.row]];
			return cell;
		}
			break;
		default: {return nil;}
			break;
	}
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	switch (collectionView.tag) {
		case COLLECTION_PALETTE: {
			PaletteCell *cell = (PaletteCell*)[collectionView cellForItemAtIndexPath:indexPath];
			[self colorSelected:cell.color];
		}
			break;
		case COLLECTION_TOOL: {
//			UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
			switch (indexPath.row) {
				case 0:
					//pen
					self.selectedColor = model.selectedColor;
					_fillMode = NO;
					_pickMode = NO;
					self.thickPenMode = NO;
					break;
				case 1:
					//fill
					if (_fillMode) {
						_fillMode = NO;
						[self populateViewWithCells];
					} else {
						_pickMode = NO;
						_fillMode = YES;
						[self populateViewWithCells];
					}
					break;
				case 2:
					//thick pen
					self.selectedColor = model.selectedColor;
					_fillMode = NO;
					_pickMode = NO;
					self.thickPenMode = YES;
					break;
				case 3:
					//undo
					model.pictureAreaData = model.pictureAreaDataUndoBuffer;
					[self.artView setNeedsDisplay];
					[self populateViewWithCells];
					break;
				case 4:
					//picker
					if (_pickMode) {
						_pickMode = NO;
						[self populateViewWithCells];
					} else {
						_fillMode = NO;
						_pickMode = YES;
						[self populateViewWithCells];
					}
					break;
				case 5:
					//eraser
					_fillMode = NO;
					_pickMode = NO;
					if (![self.selectedColor isEqual:[UIColor clearColor]]) {
						model.selectedColor = self.selectedColor;
						self.selectedColor = [UIColor clearColor];
					}
					break;
				case 6:
					//magnify
					[self.view addSubview:self.zoomLevelButtonsView];
					self.zoomLevelButtonsView.hidden = NO;
					break;
				case 7:
					//moonlight
					if ([self.gridView.backgroundColor isEqual:[UIColor darkGrayColor]]) {
						self.gridView.backgroundColor = [UIColor lightGrayColor];
						self.artView.backgroundColor = [UIColor lightGrayColor];
					} else {
						self.gridView.backgroundColor = [UIColor darkGrayColor];
						self.artView.backgroundColor = [UIColor darkGrayColor];
					}
					break;
				default:
					break;
			}
			
			
		}
			break;
		default:
			break;
	}
}

- (void)zoomLevelButtonPressed:(UIButton*)sender
{
	_insideX += (int)self.gridView.bounds.size.width/_cellSize/2;
	_insideY += (int)self.gridView.bounds.size.width/_cellSize/2;
	_cellSize = sender.tag;
	_insideX -= (int)self.gridView.bounds.size.width/_cellSize/2;
	_insideY -= (int)self.gridView.bounds.size.width/_cellSize/2;
	[self populateViewWithCells];
}



#pragma mark -
#pragma mark -
#pragma mark TOUCHES
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
    UIView *cell = [self.view hitTest:locationPoint withEvent:event];
	if (_pickMode) {
		[self colorSelected:[self.view colorAtPoint:[[touches anyObject] locationInView:self.view]]];
		_pickMode = NO;
		[self populateViewWithCells];
	} else if (_fillMode) {
		CGPoint locationPoint = [[touches anyObject] locationInView:self.gridView];
		[self fillColorFromTouchAtPoint:locationPoint];
		self.saveButton.hidden = NO;
		_fillMode = NO;
		[self.artView setNeedsDisplay];
		[self populateViewWithCells];
    } else if ([cell isDescendantOfView:self.joystickView]) {
        CGPoint insidePoint = [self.joystickView convertPoint:locationPoint fromView:self.view];
        
        
        
        _insideY += insidePoint.y / self.joystickView.frame.size.height * 8 - 4;
        _insideX += insidePoint.x / self.joystickView.frame.size.width * 8 - 4;
        [self populateViewWithCells];
	} else {
		[self touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
	}
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
	UIView *cell = [self.view hitTest:locationPoint withEvent:event];
	if (cell.tag == 99 && !joystickOn) {
		[self dataChangeFromTouchAtPoint:[self.view convertPoint:locationPoint toView:self.gridView] cell:cell];
	} else if ([cell isDescendantOfView:self.artViewFrameView]) {
		CGPoint insidePoint = [self.artView convertPoint:locationPoint fromView:self.view];
		_insideX = insidePoint.x - _xCount;
		_insideY = insidePoint.y - _yCount;
		[self populateViewWithCells];
	}
	cell = nil;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
	UIView *cell = [self.view hitTest:locationPoint withEvent:event];
	if (cell.tag == 99 && !joystickOn) {
		[self dataChangeFromTouchAtPoint:[self.view convertPoint:locationPoint toView:self.gridView] cell:cell];
	}
	[self.artView setNeedsDisplay];
	joystickOn = NO;
	cell = nil;

	if (_thickPenMode) {
		[self populateViewWithCells];
	}
}



#pragma mark 
#pragma mark 
#pragma mark data change methods

- (void)dataChangeFromTouchAtPoint:(CGPoint)locationPoint cell:(UIView*)cell {
	self.saveButton.hidden = NO;
	if (self.selectedColorAlpha) {
		cell.backgroundColor = self.selectedColor;
	} else {
		cell.backgroundColor = [UIColor clearColor];
	}
	
	if (self.thickPenMode) {
		for (int i=-1; i<2; i++) {
#warning CHANGES COLOR THE WRONG WAY ON CORNER CASES
		model.pictureAreaData = [NSMutableData dataChangePixelColorRed:_selectedColorRed
																 Green:_selectedColorGreen
																  Blue:_selectedColorBlue
																 Alpha:_selectedColorAlpha
																  forX:(locationPoint.x)/_cellSize + _insideX -1
																	 Y:(locationPoint.y)/_cellSize + _insideY +i
																ofData:model.pictureAreaData
															  forImage:self.artView.image];
		model.pictureAreaData = [NSMutableData dataChangePixelColorRed:_selectedColorRed
																 Green:_selectedColorGreen
																  Blue:_selectedColorBlue
																 Alpha:_selectedColorAlpha
																  forX:(locationPoint.x)/_cellSize + _insideX
																	 Y:(locationPoint.y)/_cellSize + _insideY +i
																ofData:model.pictureAreaData
															  forImage:self.artView.image];
		model.pictureAreaData = [NSMutableData dataChangePixelColorRed:_selectedColorRed
																 Green:_selectedColorGreen
																  Blue:_selectedColorBlue
																 Alpha:_selectedColorAlpha
																  forX:(locationPoint.x)/_cellSize + _insideX +1
																	 Y:(locationPoint.y)/_cellSize + _insideY +i
																ofData:model.pictureAreaData
															  forImage:self.artView.image];
		}
	} else {

	model.pictureAreaData = [NSMutableData dataChangePixelColorRed:_selectedColorRed
														 Green:_selectedColorGreen
														  Blue:_selectedColorBlue
														 Alpha:_selectedColorAlpha
														  forX:(locationPoint.x )/_cellSize + _insideX
															 Y:(locationPoint.y )/_cellSize + _insideY
														ofData:model.pictureAreaData
													  forImage:self.artView.image];
	}
}


- (void)fillColorFromTouchAtPoint:(CGPoint)locationPoint
{
	model.pictureAreaDataUndoBuffer = [NSMutableData dataFromImage:self.artView.image];
	model.pictureAreaData = [NSMutableData dataContourFillWithColorRed:_selectedColorRed
															 Green:_selectedColorGreen
															  Blue:_selectedColorBlue
															 Alpha:_selectedColorAlpha
															 fromX:(locationPoint.x )/_cellSize + _insideX
																 Y:(locationPoint.y )/_cellSize + _insideY
															ofData:model.pictureAreaData
														  forImage:self.artView.image];
}


#pragma mark
#pragma mark
#pragma mark  ACTIONS
- (IBAction)saveButtonPressed:(id)sender
{
	model.pictureArea = self.artView.image;
	model.picture = [model.cppManager pastePictureArea:model.pictureArea intoPicture:model.picture X:(int)model.pictureAreaRect.origin.x Y:(int)model.pictureAreaRect.origin.y];

	[model saveImage:model.picture toFile:model.fileNameFullPath];
	self.saveButton.hidden = YES;
}

- (IBAction)backToPreview:(id)sender
{
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	[self.navigationController popViewControllerAnimated:YES];
}



#pragma mark -
#pragma mark -
#pragma mark - SETTERS
- (void)setSelectedColor:(UIColor *)selectedColor {
	_selectedColor = selectedColor;
	model.selectedColor = selectedColor;
	CGFloat red;
	CGFloat green;
	CGFloat blue;
	CGFloat alpha;
	[_selectedColor getRed:&red green:&green blue:&blue alpha:&alpha];
	_selectedColorRed = (int)(red * 255);
	_selectedColorGreen = (int)(green * 255);
	_selectedColorBlue = (int)(blue * 255);
	_selectedColorAlpha = (int)(alpha * 255);
}


#pragma mark -
#pragma mark -
#pragma mark - colorSelected

- (void)colorSelected:(UIColor *)color {
	_pickMode = NO;
	_fillMode = NO;
	[self populateViewWithCells];
	self.selectedColor = color;
	self.colorView.backgroundColor = color;
}




@end