//
//  PreviewViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 12/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "PreviewController.h"
#import "EditorViewController.h"
#import "PaletteViewController.h"
#import "TransformationsViewController.h"

@interface PreviewController ()
{
	Model *model;
	CGFloat coeff;
}

@end

@implementation PreviewController

#pragma mark -
#pragma mark -
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
	model = [Model sharedInstance];
}

#pragma mark
#pragma mark
#pragma mark LIFECYCLE
- (void)viewWillAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	[self.areaView removeFromSuperview];
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self loadPicture];
	[self setupAreaView];
	
	CGRect frame = [self getFrameSizeForImage:self.pictureView.image inImageView:self.pictureView];
	self.pictureView.frame = CGRectMake(self.pictureView.frame.origin.x + frame.origin.x, self.pictureView.frame.origin.y + frame.origin.y, frame.size.width, frame.size.height);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fileNameField.delegate = self;
    self.fileNameField.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
	self.fileNameField.text = [[model.fileNameFullPath lastPathComponent] stringByDeletingPathExtension];
    self.fileNameField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    
	self.pictureView.image = model.picture;
}


- (CGRect)getFrameSizeForImage:(UIImage *)image inImageView:(UIImageView *)imageView
{
    CGFloat hfactor = image.size.width / imageView.frame.size.width;
    CGFloat vfactor = image.size.height / imageView.frame.size.height;
    CGFloat factor = fmax(hfactor, vfactor);
    CGFloat newWidth = image.size.width / factor;
    CGFloat newHeight = image.size.height / factor;
    CGFloat leftOffset = (imageView.frame.size.width - newWidth) / 2;
    CGFloat topOffset = (imageView.frame.size.height - newHeight) / 2;
    return CGRectMake(leftOffset, topOffset, newWidth, newHeight);
}




#pragma mark -
#pragma mark -
#pragma mark - setup area view

- (void)setupAreaView
{
	CGRect rect;
	coeff = MIN(self.pictureView.frame.size.width/model.picture.size.width, self.pictureView.frame.size.height/model.picture.size.height);
	if (model.picture.size.width < 100 && model.picture.size.height < 100)
	{
		rect = CGRectZero;
//	} else if (model.picture.size.width >= 100 && model.picture.size.height < 100)
//	{
//		rect = CGRectMake(0, 0, 100, model.picture.size.height);
//	} else if (model.picture.size.width < 100 && model.picture.size.height >= 100)
//	{
//		rect = CGRectMake(0, 0, model.picture.size.width, 100);
	} else
	{
		rect = CGRectMake(0, 0, 100*coeff, 100*coeff);
	}
	self.areaView = [[UIView alloc]initWithFrame:rect];
	self.areaView.backgroundColor = [UIColor clearColor];
	self.areaView.layer.borderWidth = 2.0;
	self.areaView.layer.borderColor = [UIColor purpleColor].CGColor;
	[self.pictureView addSubview:self.areaView];
	[self formPictureArea];
}

#pragma mark -
#pragma mark -
#pragma mark - LOAD PICTURE FROM FILE
- (void)loadPicture
{
	UIImage *tmp = [UIImage imageWithContentsOfFile:model.fileNameFullPath];
	if (tmp) {
		model.picture = tmp;
		self.pictureView.image = tmp;
		model.picture = tmp;
	}
	tmp = nil;
}


#pragma mark -
#pragma mark -
#pragma mark - touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint point;
	for (UITouch *touch in [touches allObjects]) {
		point = [touch locationInView:self.pictureView];
	}
	self.areaView.center = point;
	[self validateAreaViewFrame];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint point;
	for (UITouch *touch in [touches allObjects]) {
		point = [touch locationInView:self.pictureView];
	}
	self.areaView.center = point;
	[self validateAreaViewFrame];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint point;
	for (UITouch *touch in [touches allObjects]) {
		point = [touch locationInView:self.pictureView];
	}
	self.areaView.center = point;
	[self validateAreaViewFrame];
	[self formPictureArea];
}

- (void)validateAreaViewFrame
{
	CGFloat origX = self.areaView.frame.origin.x;
	CGFloat origY = self.areaView.frame.origin.y;
	CGFloat width = self.areaView.frame.size.width;
	CGFloat height = self.areaView.frame.size.height;
	CGFloat mwidth = self.pictureView.frame.size.width - width;
	CGFloat mheight = self.pictureView.frame.size.height - height;
	origX = (origX < 0)? 0 : (origX > mwidth)?  mwidth  : origX;
	origY = (origY < 0)? 0 : (origY > mheight)? mheight : origY;
	self.areaView.frame = CGRectMake(origX, origY, width, height);
}


- (void)formPictureArea
{
	model.pictureAreaRect = CGRectMake((int)self.areaView.frame.origin.x / coeff, (int)self.areaView.frame.origin.y /coeff,
									   (int)self.areaView.frame.size.width /coeff, (int)self.areaView.frame.size.height /coeff);
	if (model.picture.size.width <=100 && model.picture.size.height <=100) {
		model.pictureArea = model.picture;
	} else	model.pictureArea = [UIImage imageWithCGImage:CGImageCreateWithImageInRect(model.picture.CGImage, model.pictureAreaRect)];
}


#pragma mark -
#pragma mark -
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.fileName = [[[self.fileNameField.text mutableCopy] stringByAppendingPathExtension:@"png"] mutableCopy];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self renameFile:self.fileName toFile:[[self.fileNameField.text mutableCopy] stringByAppendingPathExtension:@"png"]];
    self.fileName = [[[self.fileNameField.text mutableCopy] stringByAppendingPathExtension:@"png"] mutableCopy];
    [self.fileNameField resignFirstResponder];
    return YES;
}

- (void)renameFile:(NSString*)fromFile toFile:(NSString*)toFile
{
    NSError *error;
    [[NSFileManager defaultManager] moveItemAtPath:[self.filesDirectory stringByAppendingPathComponent:fromFile]
                                            toPath:[self.filesDirectory stringByAppendingPathComponent:toFile] error:&error];
    if (!error) {
        self.fileName = [toFile mutableCopy];
    }
    //	[self.superview performSelector:@selector(reloadData) withObject:nil];
}



@end
