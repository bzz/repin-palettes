//
//  PaletteViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 17/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "PaletteViewController.h"
#import "PaletteCell.h"
#import "EditorViewController.h"


@interface PaletteViewController () {
	Model *model;
}

@end

@implementation PaletteViewController


#pragma mark
#pragma mark
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit
{
	model = [Model sharedInstance];
	self.colorView.backgroundColor = model.selectedColor;
	self.saveButton.hidden = YES;
	self.savePaletteButton.hidden = YES;
	[model.paletteColorsArray removeAllObjects];
}

#pragma mark
#pragma mark
#pragma mark LIFECYCLE
- (void)viewWillAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	self.saveButton.hidden = YES;
	self.savePaletteButton.hidden = YES;
	[self showLockButton];
}

- (void)viewDidAppear:(BOOL)animated
{
	[model formPaletteColorsArray];
	[self.paletteCollectionView setContentOffset:CGPointZero animated:YES];
	[self.paletteCollectionView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.redSlider.delegate = self;
    self.greenSlider.delegate = self;
    self.blueSlider.delegate = self;
    self.redSlider.color = 1;
    self.greenSlider.color = 2;
    self.blueSlider.color = 3;
    self.redSlider.valueLabel.textColor = [UIColor redColor];
    self.greenSlider.valueLabel.textColor = [UIColor greenColor];
    self.blueSlider.valueLabel.textColor = [UIColor blueColor];
    self.redSlider.value = model.selectedColorRed;
    self.greenSlider.value = model.selectedColorGreen;
    self.blueSlider.value = model.selectedColorBlue;

    self.pictureView.image = model.picture;
	self.paletteCollectionView.delegate = self;
	self.paletteCollectionView.dataSource = self;
	self.paletteCollectionView.allowsMultipleSelection = YES;
	[self.paletteCollectionView registerClass:[PaletteCell class] forCellWithReuseIdentifier:@"paletteCell"];
}


- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (void)showLockButton
{
	if (![[NSUserDefaults standardUserDefaults] valueForKey:@"rated"] && [[[NSUserDefaults standardUserDefaults] valueForKey:@"run"] integerValue] > 5 ) {
		self.lockButton.hidden = NO;
	}
}

#pragma mark
#pragma mark
#pragma mark COLLECTION VIEW DELEGATES

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	switch (section) {
		case 0:
			return model.paletteColorsArray.count;
			break;
		case 1:
			return model.paletteUserColorsArray.count;
			break;
		default:
			break;
	}
	return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	PaletteCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"paletteCell" forIndexPath:indexPath];
	switch (indexPath.section) {
		case 0:
			cell.color = model.paletteColorsArray[indexPath.row];
			break;
		case 1:
			cell.color = model.paletteUserColorsArray[indexPath.row];
			break;
		default:
			break;
	}
	return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	PaletteCell *cell = (PaletteCell*)[collectionView cellForItemAtIndexPath:indexPath];
	model.selectedColor = cell.color;
	self.selectedColor = cell.color;
	self.redSlider.value = model.selectedColorRed;
	self.greenSlider.value = model.selectedColorGreen;
	self.blueSlider.value = model.selectedColorBlue;
	self.colorView.backgroundColor = cell.color;

	cell.layer.borderWidth = 3;
	cell.layer.borderColor = collectionView.backgroundColor.CGColor;
	cell.selected = YES;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
	PaletteCell *cell = (PaletteCell*)[collectionView cellForItemAtIndexPath:indexPath];
	model.selectedColor = cell.color;
	self.redSlider.value = model.selectedColorRed;
	self.greenSlider.value = model.selectedColorGreen;
	self.blueSlider.value = model.selectedColorBlue;
	self.colorView.backgroundColor = cell.color;
	
	cell.layer.borderWidth = 0;
	cell.selected = NO;
}

#pragma mark
#pragma mark
#pragma mark ACTIONS

- (IBAction)backButtonPressed:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)saveButtonPressed:(UIButton *)sender {
	[model saveImage:self.pictureView.image toFile:model.fileNameFullPath];
	model.picture = self.pictureView.image;
	self.saveButton.hidden = YES;
}

- (IBAction)savePaletteButtonPressed:(id)sender {
	CGFloat red;
	CGFloat green;
	CGFloat blue;
	CGFloat alpha;
	NSMutableData *data = [NSMutableData new];
	for (UIColor *color in model.paletteUserColorsArray) {
		[color getRed:&red green:&green blue:&blue alpha:&alpha];
		unsigned char bytesToAppend[4] = {
			(unsigned char)(red * 255),
			(unsigned char)(green * 255),
			(unsigned char)(blue * 255),
			(unsigned char)(alpha * 255)
		};
		[data appendBytes:bytesToAppend length:sizeof(bytesToAppend)];
	}
	UIImage *emptyImage = [UIImage imageWithColor:[UIColor clearColor] andSize:CGSizeMake(model.paletteUserColorsArray.count, 1)];
	model.paletteImage = [UIImage imageWithData:data forImage:emptyImage];
	NSString *fileName = [[model.fileNameFullPath lastPathComponent] stringByDeletingPathExtension];
	NSString *fullFileName = [[model.documentsDirectory  stringByAppendingPathComponent:fileName] stringByAppendingPathExtension:@"palette"];
	[model saveImage:model.paletteImage toFile:fullFileName];
	[[NSUserDefaults standardUserDefaults] setValue:fullFileName forKey:@"palette"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	self.savePaletteButton.hidden = YES;
}

- (IBAction)mergeButtonPressed:(id)sender {
	NSMutableArray *arr_merge = [NSMutableArray new];
	for (PaletteCell *cell in self.paletteCollectionView.visibleCells) {
		if (cell.isSelected) {
			for (UIColor *color in model.paletteColorsArray) {
				if ([color isEqual:cell.color]) {
					[arr_merge addObject:color];
					self.saveButton.hidden = NO;
				}
			}
		}
	}
	[model.paletteColorsArray removeObjectsInArray:arr_merge];
	if (![model colorExistsInPaletteColorsArray:model.selectedColor] && arr_merge.count) {
		[model.paletteColorsArray addObject:model.selectedColor];
	}
	model.picture = [model.cppManager mergeColorsForPicture:model.picture newColor:model.selectedColor colorsArray:arr_merge];
	self.pictureView.image = model.picture;
	[self.paletteCollectionView reloadData];
}


- (IBAction)deleteButtonPressed:(id)sender {
	NSMutableArray *arr_del = [NSMutableArray new];
	for (PaletteCell *cell in self.paletteCollectionView.visibleCells) {
		if (cell.isSelected) {
			for (UIColor *color in model.paletteUserColorsArray) {

				if ([color isEqual:cell.color]) {
					[arr_del addObject:color];
					self.savePaletteButton.hidden = NO;
				}
			}
		}
	}
	[model.paletteUserColorsArray removeObjectsInArray:arr_del];
	arr_del=nil;
	[self.paletteCollectionView reloadData];
}


- (IBAction)addNewColorButtonPressed:(id)sender
{
	if (![model colorExistsInUserPalette:_selectedColor]) {
		[model.paletteUserColorsArray addObject:_selectedColor];
		self.savePaletteButton.hidden = NO;
		[self.paletteCollectionView reloadData];
	}
}

- (IBAction)addColorToUserPaletteButtonPressed:(id)sender
{
	for (PaletteCell *cell in self.paletteCollectionView.visibleCells) {
		if (cell.isSelected && ![model colorExistsInUserPalette:cell.color]) {
			[model.paletteUserColorsArray addObject:cell.color];
			self.savePaletteButton.hidden = NO;
		}
	}
	[self.paletteCollectionView reloadData];
}


- (void)colorBytePicker:(ColorBytePicker *)colorBytePicker valueSet:(NSInteger)value {
    switch (colorBytePicker.color) {
        case 1:
            model.selectedColorRed = (int)_redSlider.value;
            break;
        case 2:
            model.selectedColorGreen = (int)_greenSlider.value;
            break;
        case 3:
            model.selectedColorBlue = (int)_blueSlider.value;
            break;
        default:
            break;
    }
    self.selectedColor = model.selectedColor;    
}


- (IBAction)lockButtonPressed:(UIButton *)sender {
	UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"\ue502 This app is one man project \ue502" message:@"Want it to get better?\n\ue32f\ue32f\ue32f\ue32f\ue32f\nRATE IT!" delegate:self  cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alert addButtonWithTitle:@"Rate"];
    [alert show];
}
#pragma mark -
#pragma mark -
#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 1) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id889450462"]];
		[[NSUserDefaults standardUserDefaults] setValue:@YES forKey:@"rated"];
	}
}

#pragma mark -
#pragma mark -
#pragma mark - touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint point;
	for (UITouch *touch in [touches allObjects]) {
        if (touch.view == self.redSlider) {
            [self.redSlider touchesBegan:touches withEvent:event];
        } else if (touch.view == self.greenSlider) {
            [self.greenSlider touchesBegan:touches withEvent:event];
        } else if (touch.view == self.blueSlider) {
            [self.blueSlider touchesBegan:touches withEvent:event];
        } else {
            point = [touch locationInView:self.pictureView];
            model.selectedColor = [self.pictureView colorAtPoint:point];
            self.selectedColor = [self.pictureView colorAtPoint:point];
            self.redSlider.value = model.selectedColorRed;
            self.greenSlider.value = model.selectedColorGreen;
            self.blueSlider.value = model.selectedColorBlue;
            self.colorView.backgroundColor = [self.pictureView colorAtPoint:point];
        }
	}
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self touchesBegan:touches withEvent:event];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self touchesBegan:touches withEvent:event];
}



#pragma mark -
#pragma mark -
#pragma mark - SETTERS

- (void)setSelectedColor:(UIColor *)selectedColor {
	_selectedColor = selectedColor;
	self.colorView.backgroundColor = _selectedColor;
}


@end
