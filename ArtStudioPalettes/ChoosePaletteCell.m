//
//  ChoosePaletteCell.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 27/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "ChoosePaletteCell.h"

@implementation ChoosePaletteCell

- (void)prepareForReuse {
	[super prepareForReuse];
}



- (id)init
{
	if (self=[super init]) {
		[self setupInit];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ([super initWithCoder:aDecoder]) {
		[self setupInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if ([super initWithFrame:frame]) {
		[self setupInit];
    }
    return self;
}

- (void)setupInit {
}


@end
