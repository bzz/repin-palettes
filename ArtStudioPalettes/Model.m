//
//  PictureModel.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 26/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "Model.h"




@interface Model () {
}

@end


@implementation Model



+ (instancetype)sharedInstance {
    static Model *shared;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        shared = [self new];
    });
    return shared;
}


- (id)init {
	if (self = [super init]) {
		self.cppManager = [CPPManager new];
		self.picture = [UIImage new];
		self.pictureArea = [UIImage new];
		self.pictureAreaData = [NSMutableData new];
		self.pictureAreaDataUndoBuffer = [NSMutableData new];
		self.paletteImage = [UIImage new];
		self.data = [NSMutableData new];
		self.selectedColor = [UIColor new];
		self.selectedColorRed = 100;
		self.selectedColorGreen = 100;
		self.selectedColorBlue = 100;
		self.selectedColorAlpha = 255;
		self.paletteUserColorsArray = [NSMutableArray new];
		self.fileManager = [NSFileManager defaultManager];
		self.documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//		self.Q = dispatch_queue_create("com.bzz.background_queue", 0);
	}
	return self;
}


- (void)formPaletteColorsArray {
	if (!self.paletteColorsArray) {
		self.paletteColorsArray = [NSMutableArray new];
	} else [self.paletteColorsArray removeAllObjects];

//	Palette *pal = [[Picture alloc]initWithImage:self.picture].palette;
//	NSArray *arr = [pal sort];
//	for (Color *c in arr) {
//		[self.paletteColorsArray addObject:c.uicolor];
//	}
	self.paletteColorsArray = [self.cppManager dataPaletteDataFromImage:self.picture];
}

- (void)formPaletteUserColorsArrayFromImage:(UIImage *)image {
	if (!self.paletteUserColorsArray) {
		self.paletteUserColorsArray = [NSMutableArray new];
	} else [self.paletteUserColorsArray removeAllObjects];
	
//	Palette *pal = [[Picture alloc]initWithImage:image].palette;
//	NSArray *arr = [pal sort];
//	for (Color *c in arr) {
//		[self.paletteUserColorsArray addObject:c.uicolor];
//	}
	self.paletteUserColorsArray = [self.cppManager dataPaletteDataFromImage:image];
}



#pragma mark -
#pragma mark -
#pragma mark - SETTERS
- (void)setSelectedColor:(UIColor *)selectedColor
{
	_selectedColor = selectedColor;
	CGFloat red;
	CGFloat green;
	CGFloat blue;
	CGFloat alpha;
	[selectedColor getRed:&red green:&green blue:&blue alpha:&alpha];
	_selectedColorRed = (int)(red * 255);
	_selectedColorGreen = (int)(green * 255);
	_selectedColorBlue = (int)(blue * 255);
	_selectedColorAlpha = (int)(alpha * 255);
}

- (void)setSelectedColorRed:(int)selectedColorRed
{
	_selectedColorRed = selectedColorRed;
	[self updateSelectedColor];
}
- (void)setSelectedColorGreen:(int)selectedColorGreen
{
	_selectedColorGreen = selectedColorGreen;
	[self updateSelectedColor];
}
- (void)setSelectedColorBlue:(int)selectedColorBlue
{
	_selectedColorBlue = selectedColorBlue;
	[self updateSelectedColor];
}
- (void)setSelectedColorAlpha:(int)selectedColorAlpha
{
	_selectedColorAlpha = selectedColorAlpha;
	[self updateSelectedColor];
}
- (void)updateSelectedColor {
	_selectedColor = [UIColor colorWithRed:(CGFloat)_selectedColorRed/255 green:(CGFloat)_selectedColorGreen/255 blue:(CGFloat)_selectedColorBlue/255 alpha:(CGFloat)_selectedColorAlpha/255];
}

- (BOOL)colorExistsInUserPalette:(UIColor*)color
{
	if (!color) {
		return YES;
	}
	for (UIColor *aColor in self.paletteUserColorsArray) {
		if ([aColor isEqual:color]) {
			return YES;
		}
	}
	return NO;
}

- (BOOL)colorExistsInPaletteColorsArray:(UIColor*)color
{
	for (UIColor *aColor in self.paletteColorsArray) {
		if ([aColor isEqual:color]) {
			return YES;
		}
	}
	return NO;
}


- (void)saveImage:(UIImage*)image toFile:(NSString *)fileName
{
	if (![self.fileManager fileExistsAtPath:self.documentsDirectory]){
		[self.fileManager createDirectoryAtPath:self.documentsDirectory withIntermediateDirectories:NO attributes:nil error:nil];
	}
	NSData * data = UIImagePNGRepresentation(image);
	[data writeToFile:fileName atomically:YES];
	data = nil;
}


#pragma mark
#pragma mark
#pragma mark SETTERS

- (void)setPicture:(UIImage *)picture
{
	if (picture) {
		_picture = picture;
		self.data = [NSMutableData dataFromImage:self.picture];
	}
}


@end
