//
//  ProjectCell.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 10/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *pictureView;
@property (weak, nonatomic) IBOutlet UITextField *projectNameField;
@property (weak, nonatomic) IBOutlet UILabel *filesCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end
