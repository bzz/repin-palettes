//
//  FileManagerViewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 09/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreviewController.h"
#import <MessageUI/MessageUI.h>


@interface FileManagerViewController : UIViewController
<UICollectionViewDataSource,
UICollectionViewDelegate,
MFMailComposeViewControllerDelegate,
UITextFieldDelegate
>

@property (strong, nonatomic) NSString *filesDirectory;
@property (weak, nonatomic) IBOutlet UITextField *projectNameField;
@property (strong, nonatomic) NSMutableString *projectName;


@end
