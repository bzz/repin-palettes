//
//  FileManagerViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 09/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "FileManagerViewController.h"
#import "FileCell.h"

@interface FileManagerViewController () {
	Model *model;
}

@property (weak, nonatomic) IBOutlet UICollectionView *fileList;
@property (strong, nonatomic) NSMutableArray *filenamesArray;
@property (strong, nonatomic) NSMutableArray *iconsArray;

@end

@implementation FileManagerViewController

#pragma mark -
#pragma mark -
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
	model = [Model sharedInstance];
}

#pragma mark -
#pragma mark -
#pragma mark - LIFE
- (void)viewDidAppear:(BOOL)animated {
	[self formFilenamesArray];
	[self.fileList reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
	self.fileList.dataSource = self;
	self.fileList.delegate = self;
    self.projectNameField.delegate = self;
    self.projectNameField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    self.projectNameField.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
    self.projectNameField.text = [self.filesDirectory lastPathComponent];

	if (![model.fileManager contentsOfDirectoryAtPath:self.filesDirectory error:nil].count) {
		if (model.picture) {
			[self saveImage:model.picture toFile:@"untitled.png"];
		} else {
		[self saveImage:[UIImage imageNamed:@"blank.png"] toFile:@"untitled.png"];
		}
	}

	[self formFilenamesArray];
}

- (void)formFilenamesArray {
	if (self.filenamesArray) {
		[self.filenamesArray removeAllObjects];
		[self.iconsArray removeAllObjects];
	} else 	{
		self.filenamesArray = [NSMutableArray new];
		self.iconsArray = [NSMutableArray new];
	}
	NSArray *array = [model.fileManager contentsOfDirectoryAtPath:self.filesDirectory error:nil];
	if (array.count) {
		for (NSString *file in array) {
			if ([file.pathExtension isEqual:@"png"]) {
				BOOL isDirectory = NO;
				if ([model.fileManager fileExistsAtPath:[self.filesDirectory stringByAppendingPathComponent:file] isDirectory:&isDirectory]) {
					UIImage *tmp = [UIImage imageWithContentsOfFile:[self.filesDirectory stringByAppendingPathComponent:file]];
					if (tmp) {
						[self.filenamesArray addObject:file];
						[self.iconsArray addObject:tmp];
					}
				}
			}
		}
	}
	array = nil;
}


- (void)saveImage:(UIImage*)image toFile:(NSString *)fileName {
	if (![[NSFileManager defaultManager] fileExistsAtPath:self.filesDirectory]){
		[[NSFileManager defaultManager] createDirectoryAtPath:self.filesDirectory withIntermediateDirectories:NO attributes:nil error:nil];
	}
	[UIImagePNGRepresentation(image) writeToFile:[self.filesDirectory  stringByAppendingPathComponent:fileName] atomically:YES];
}


#pragma mark -
#pragma mark -
#pragma mark Delegates

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *collectionViewCellIdentifier = @"Cell";
    
    FileCell *cell = (FileCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellIdentifier forIndexPath:indexPath];
	cell.fileNameField.text = [self.filenamesArray[indexPath.row] stringByDeletingPathExtension];
	cell.pictureView.image = self.iconsArray[indexPath.row];
	
	cell.sendButton.tag = indexPath.row;
	cell.makeCopyButton.tag = indexPath.row;
	cell.deleteButton.tag = indexPath.row;
	
    cell.fileNameField.userInteractionEnabled = NO;
    
	return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.filenamesArray.count;
}



#pragma mark -
#pragma mark -
#pragma mark ACTIONS
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	model.fileNameFullPath = [self.filesDirectory stringByAppendingPathComponent:self.filenamesArray[indexPath.row]];
	model.picture = self.iconsArray[indexPath.row];
	[self performSegueWithIdentifier:@"toPreview" sender:self];
}

- (IBAction)deleteButtonPressed:(UIButton *)sender {
	if (sender.selected) {
		sender.selected = NO;
		[self deleteFileAtRow:sender.tag];
	} else {
		sender.backgroundColor = [sender.backgroundColor inverseColor];
		sender.selected = YES;
		[sender performSelector:@selector(setBackgroundColor:) withObject:[sender.backgroundColor inverseColor] afterDelay:1.0];
		[sender performSelector:@selector(setSelected:) withObject:@"NO" afterDelay:1.0];
	}
}
- (void)deleteFileAtRow:(NSInteger)row {
	[self.self.fileList performBatchUpdates:^{
		if (row < self.filenamesArray.count) {
			[model.fileManager removeItemAtPath:[self.filesDirectory stringByAppendingPathComponent:self.filenamesArray[row]] error:nil];
		}
		[self.filenamesArray removeObjectAtIndex:row];
		[self.iconsArray removeObjectAtIndex:row];
		NSIndexPath *indexPath =[NSIndexPath indexPathForRow:row inSection:0];
		[self.fileList deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
		[self.fileList reloadData];
    }];
}

- (IBAction)copyButtonPressed:(UIButton*)sender {
	NSString *copyFrom = [self.filesDirectory stringByAppendingPathComponent:self.filenamesArray[sender.tag]];
	NSInteger i =0;
	NSString *copyTo;
	do {
		i++;
		NSString *fileName = [[copyFrom stringByDeletingPathExtension] stringByAppendingFormat:@"%i", i];
		copyTo = [fileName stringByAppendingPathExtension:@"png"];
	}
	while ([model.fileManager fileExistsAtPath:copyTo]);
	[model.fileManager copyItemAtPath:copyFrom toPath:copyTo error:nil];
	[self formFilenamesArray];
	[self.fileList reloadData];
	[self.fileList setContentOffset:CGPointMake(0, self.fileList.contentSize.height) animated:YES];
}


- (IBAction)sendButtonPressed:(UIButton*)sender {
	MFMailComposeViewController *composer = [[MFMailComposeViewController alloc]init];
	composer.mailComposeDelegate = self;
	NSArray *array = [NSArray arrayWithObject:@"<m.baynov@gmail.com>\32"];
	[composer setCcRecipients:array];
	[composer setSubject:[NSString stringWithFormat: @"Pic from RPv.%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]]];
	[composer setMessageBody:@"See attach:" isHTML:NO];
	NSData *data = UIImagePNGRepresentation(self.iconsArray[sender.tag]);
	[composer addAttachmentData:data mimeType:@"image/png" fileName:self.filenamesArray[sender.tag]];
	data = nil;
	array = nil;
	[self presentViewController:composer animated:YES completion:nil];
}

#pragma mark -
#pragma mark -
#pragma mark - MailComposeController delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark -
#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.projectName = [self.projectNameField.text mutableCopy];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self renameDir:self.projectName toDir:self.projectNameField.text];
    self.projectName = self.projectNameField.text.mutableCopy;
    [self.projectNameField resignFirstResponder];
    return YES;
}

- (void)renameDir:(NSString*)fromDir toDir:(NSString*)toDir {
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error;
    [[NSFileManager defaultManager] moveItemAtPath:[documentsDirectory stringByAppendingPathComponent:fromDir]
                                            toPath:[documentsDirectory stringByAppendingPathComponent:toDir] error:&error];
    if (!error) {
        self.projectName = [toDir mutableCopy];
    }
}

#pragma mark
#pragma mark
#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"toPreview"]) {
        PreviewController *vc = segue.destinationViewController;
        vc.filesDirectory = self.filesDirectory;
    }
}


@end
