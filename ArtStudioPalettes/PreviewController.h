//
//  PreviewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 12/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface PreviewController : UIViewController
<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *pictureView;
@property (strong, nonatomic) UIView *areaView;
@property (weak, nonatomic) IBOutlet UITextField *fileNameField;
@property (strong, nonatomic) NSMutableString *fileName;
@property (strong, nonatomic) NSString *filesDirectory;

@end
