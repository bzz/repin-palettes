//
//  AppDelegate.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 09/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "AppDelegate.h"
#import "Model.h"



@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
	[[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                            [UIFont fontWithName:@"HelveticaNeue-Thin" size:20.0], NSFontAttributeName, nil]];
	[Model sharedInstance];
	
	if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"activated"] isEqual:@YES]) {
//		[self copyBundleFilesOfType:@"sample.png" toFolder:@"Amateur samples"];
		[self copyBundleFilesOfType:@"pro.png" toFolder:@"Pro samples"];
		[self copyBundleFilesOfType:@"palette" toFolder:@"/"];
		[[NSUserDefaults standardUserDefaults] setValue:@YES forKey:@"activated"];
	}
	
	NSInteger a = [[[NSUserDefaults standardUserDefaults] valueForKey:@"run"] integerValue];
	
	[[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:++a] forKey:@"run"];
//	[[NSUserDefaults standardUserDefaults]synchronize];
//	NSLog(@" run number %li", (long)a);
	
	
//	Picture *p = [[Picture alloc]initWithImage:[UIImage imageNamed:@"face.pro.png"]];
//	UIImage *i = [p upscaleBy:5].uiimage;
//	Palette *pal = [[Palette alloc]initWithPicture:p];
	
	
    return YES;
}


- (void) copyBundleFilesOfType:(NSString*)type toFolder:(NSString*)folder
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *files = [[NSBundle mainBundle] pathsForResourcesOfType:type inDirectory:@"/"];
	NSString *dir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:folder];
	[[NSURL fileURLWithPath:dir] setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: nil];  //do not backup to iCloud

	[fileManager createDirectoryAtPath:dir withIntermediateDirectories:NO attributes:nil error:nil];
	for (int i = 0; i < files.count; i++) {
		NSString *docPdfFilePath = [dir stringByAppendingPathComponent: [files[i] lastPathComponent] ];
		[fileManager copyItemAtPath: files[i] toPath:docPdfFilePath  error:nil];
	}
}


- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
