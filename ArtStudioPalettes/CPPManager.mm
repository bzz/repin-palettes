//
//  CPPManager.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 24/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "CPPManager.h"
#import "pixel_data.h"

@implementation CPPManager
{
}

- (id)init
{
	if (self = [super init]) {
//		self.data = [NSMutableData new];
	}
	return self;
}


- (NSMutableArray *)dataPaletteDataFromImage:(UIImage *)picture
{
	if (picture) {
		Pic pic = [self picFromUIImage:picture];
		Line cvec = cpp->palette(pic);
		NSMutableArray *colorsArray = [NSMutableArray new];
		
		for (int i = 0; i < cvec.size(); i++)
		{
			[colorsArray addObject:[UIColor colorWithRed:(CGFloat)cvec[i].red/255.0f
												   green:(CGFloat)cvec[i].green/255.0f
													blue:(CGFloat)cvec[i].blue/255.0f
												   alpha:(CGFloat)cvec[i].alpha/255.0f]];
		}
		return colorsArray;
	} else return nil;
}

- (UIImage*)mergeColorsForPicture:(UIImage*)picture newColor:(UIColor*)newColor colorsArray:(NSArray*)colorsArray
{
	UIImage *newImage;
	if (picture) {
		Pic pic = [self picFromUIImage:picture];
		Color ncolor = [self colorFromUIColor:newColor];
		Line cvec;
		cvec.empty();
		for (UIColor* aColor in colorsArray) {
			cvec.emplace_back([self colorFromUIColor:aColor]);
		}

		Pic out = cpp->merge_image_ncolor_colors(pic, ncolor, cvec);
		newImage = [self imageFromPic:out forImage:picture];
	}
	return newImage;
}


- (UIImage*)pastePictureArea:(UIImage*)pictureArea intoPicture:(UIImage*)picture X:(int)x Y:(int)y
{
	UIImage *newImage;
	if (picture && pictureArea) {
		Pic pic = [self picFromUIImage:picture];
		Pic pic1 = [self picFromUIImage:pictureArea];
		Pic out = cpp -> paste_picture_area(pic1, pic, x, y);
		newImage = [self imageFromPic:out forImage:picture];
	}
	return newImage;
}

- (UIImage *)cropAddPixelsToPicture:(UIImage *)picture left:(int)left right:(int)right top:(int)top bottom:(int)bottom
{
	UIImage *newImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(picture.size.width+left+right, picture.size.height+top+bottom)];
	if (picture) {
		Pic pic = [self picFromUIImage:picture];
		Pic out = cpp -> crop_add_pixels(pic, left, right, top, bottom);
		newImage = [self imageFromPic:out forImage:newImage];
	}
	return newImage;
}


- (UIImage *)flipPicture:(UIImage *)picture
{
	UIImage *newImage;
	if (picture) {
		Pic pic = [self picFromUIImage:picture];
		Pic out = cpp -> flip_picture(pic);
		newImage = [self imageFromPic:out forImage:picture];
	}
	return newImage;
}

- (UIImage *)rotatePicture:(UIImage *)picture
{
	UIImage *newImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(picture.size.height, picture.size.width)];
	if (picture) {
		Pic pic = [self picFromUIImage:picture];
		Pic out = cpp -> rotate_picture(pic);
		newImage = [self imageFromPic:out forImage:newImage];
	}
	return newImage;
}





- (UIImage *)applyTransform:(NSInteger)transform toPicture:(UIImage *)picture
{
	if (picture) {
		UIImage *newImage;
		switch (transform) {
			case 0:
				newImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake((CGFloat)(int)picture.size.width/2, (CGFloat)(int)picture.size.height/2)];
				if (picture) {
					Pic pic = [self picFromUIImage:picture];
					Pic out = cpp -> downscale2x(pic);
					newImage = [self imageFromPic:out forImage:newImage];
				}
				return newImage;
				break;
			case 1:
				newImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(picture.size.width*2, picture.size.height*2)];
				if (picture) {
					Pic pic = [self picFromUIImage:picture];
					Pic out = cpp -> interpolate2x(pic);
					newImage = [self imageFromPic:out forImage:newImage];
				}
				return newImage;
				break;
			case 2:
				newImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(picture.size.width*2, picture.size.height*2)];
				if (picture) {
					Pic pic = [self picFromUIImage:picture];
					Pic out = cpp -> upscale2x(pic);
					newImage = [self imageFromPic:out forImage:newImage];
				}
				return newImage;
				break;
			case 3:
				newImage = [UIImage imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(picture.size.width*3, picture.size.height*3)];
				if (picture) {
					Pic pic = [self picFromUIImage:picture];
					Pic out = cpp -> upscale3x(pic);
					newImage = [self imageFromPic:out forImage:newImage];
				}
				return newImage;
				break;
				
			default:
				return picture;
				break;
		}
	} else return picture;
}











#pragma mark
#pragma mark
#pragma mark Helper methods

- (Color)colorFromUIColor:(UIColor*)color
{
	struct Color c;
	CGFloat red;
	CGFloat green;
	CGFloat blue;
	CGFloat alpha;
	[color getRed:&red green:&green blue:&blue alpha:&alpha];
	c.red = (unsigned char)(red*255);
	c.green = (unsigned char)(green*255);
	c.blue = (unsigned char)(blue*255);
	c.alpha = (unsigned char)(alpha*255);
	return c;
}

- (Pic)picFromUIImage:(UIImage*)image
{
	NSMutableData* data = [NSMutableData dataFromImage:image];
	Pic pic ((size_t)image.size.width, (size_t)image.size.height, (Color*)[data bytes]);
	return pic;
}

- (UIImage*)imageFromPic:(Pic)pic forImage:(UIImage*)image
{
	unsigned char* d = (unsigned char*)pic.data;
	NSMutableData* data = [NSMutableData dataWithBytes:d length:(NSUInteger)pic.dsize];
	UIImage *img = [UIImage imageWithData:data forImage:image];
	return img;
}


@end
