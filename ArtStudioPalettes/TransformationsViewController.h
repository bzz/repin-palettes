//
//  TransformationsViewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 13/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TransformationsViewController : UIViewController
<UICollectionViewDataSource,
UICollectionViewDelegate,
UIAlertViewDelegate>



@property (weak, nonatomic) IBOutlet UIImageView *pictureView;
@property (weak, nonatomic) IBOutlet UICollectionView *transformationsCollectionView;

@property (weak, nonatomic) IBOutlet UITextField *pixelsLeftField;
@property (weak, nonatomic) IBOutlet UITextField *pixelsRightField;
@property (weak, nonatomic) IBOutlet UITextField *pixelsTopField;
@property (weak, nonatomic) IBOutlet UITextField *pixelsBottomField;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UITextField *dimensionsTextField;


@property (nonatomic, strong) NSMutableArray *transformationsArray;

@property (nonatomic) CGFloat pinStartX;
@property (nonatomic) CGFloat pinStartY;

@property (nonatomic) CGFloat pinEndX;
@property (nonatomic) CGFloat pinEndY;


@end
