//
//  ChoosePaletteViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 27/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "ChoosePaletteViewController.h"
#import "ChoosePaletteCell.h"
#import "PaletteViewController.h"

@interface ChoosePaletteViewController ()
{
	Model *model;
	UIImage *emptyImage;
	NSString *fileNameToPass;
	UIImage *pictureToPass;
}

@property (weak, nonatomic) IBOutlet UICollectionView *fileList;
@property (strong, nonatomic) NSMutableArray *filenamesArray;
@property (strong, nonatomic) NSMutableArray *iconsArray;

@end

@implementation ChoosePaletteViewController

#pragma mark -
#pragma mark -
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
	model = [Model sharedInstance];
	model.fileManager = [NSFileManager defaultManager];
	model.documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	emptyImage = [UIImage imageWithColor:[UIColor yellowColor] andSize:CGSizeMake(40, 40)];
}

#pragma mark -
#pragma mark -
#pragma mark - LIFE
- (void)viewWillAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated
{
	[self formFilenamesArray];
	[self.fileList reloadData];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.fileList.dataSource = self;
	self.fileList.delegate = self;
	[self formFilenamesArray];
}

- (void)formFilenamesArray
{
	if (self.filenamesArray) {
		[self.filenamesArray removeAllObjects];
		[self.iconsArray removeAllObjects];
	} else 	{
		self.filenamesArray = [NSMutableArray new];
		self.iconsArray = [NSMutableArray new];
	}
	NSArray *array = [model.fileManager contentsOfDirectoryAtPath:model.documentsDirectory error:nil];
	if (array.count) {
		for (NSString *file in array) {
			if ([file.pathExtension isEqual:@"palette"]) {
				BOOL isDirectory = NO;
				if ([model.fileManager fileExistsAtPath:[model.documentsDirectory stringByAppendingPathComponent:file] isDirectory:&isDirectory]) {
					UIImage *tmp = [UIImage imageWithContentsOfFile:[model.documentsDirectory stringByAppendingPathComponent:file]];
					if (tmp) {
						[self.filenamesArray addObject:file];
						[self.iconsArray addObject:tmp];
					}
				}
			}
		}
	}
	array = nil;
}




#pragma mark -
#pragma mark -
#pragma mark Delegates

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *collectionViewCellIdentifier = @"Cell";
    ChoosePaletteCell *cell = (ChoosePaletteCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellIdentifier forIndexPath:indexPath];
	cell.backgroundColor = [UIColor lightGrayColor];
	cell.fileNameLabel.text = [self.filenamesArray[indexPath.row] stringByDeletingPathExtension];
	cell.pictureView.image = self.iconsArray[indexPath.row];
	cell.deleteButton.tag = indexPath.row;
	return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.filenamesArray.count;
}



#pragma mark -
#pragma mark -
#pragma mark ACTIONS
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	ChoosePaletteCell *cell = (ChoosePaletteCell*)[collectionView cellForItemAtIndexPath:indexPath];
	model.paletteImage = cell.pictureView.image;
	[model formPaletteUserColorsArrayFromImage:model.paletteImage];
	cell.deleteButton.tag = indexPath.row;
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteButtonPressed:(UIButton *)sender {
	if (sender.selected) {
		sender.selected = NO;
		[self deleteFileAtRow:sender.tag];
	} else {
		sender.backgroundColor = [sender.backgroundColor inverseColor];
		sender.selected = YES;
		[sender performSelector:@selector(setBackgroundColor:) withObject:[sender.backgroundColor inverseColor] afterDelay:1.0];
		[sender performSelector:@selector(setSelected:) withObject:@"NO" afterDelay:1.0];
	}
}
- (void)deleteFileAtRow:(NSInteger)row {
	[self.self.fileList performBatchUpdates:^{
		if (row < self.filenamesArray.count) {
			[model.fileManager removeItemAtPath:[model.documentsDirectory stringByAppendingPathComponent:self.filenamesArray[row]] error:nil];
		}
		[self.filenamesArray removeObjectAtIndex:row];
		[self.iconsArray removeObjectAtIndex:row];
		NSIndexPath *indexPath =[NSIndexPath indexPathForRow:row inSection:0];
		[self.fileList deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
		[self.fileList reloadData];
    }];
}





@end
