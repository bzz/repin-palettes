//
//  EditorViewController.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 12/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AView.h"
#import "Model.h"

@interface EditorViewController : UIViewController
<UICollectionViewDataSource,
UICollectionViewDelegate>



@property (weak, nonatomic) IBOutlet UICollectionView *toolsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *paletteCollectionView;
@property (nonatomic, weak) IBOutlet UIView *gridView;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UIView *artViewFrameView;
@property (weak, nonatomic) IBOutlet UIImageView *joystickView;
@property (nonatomic, strong) AView *artView;

@property (nonatomic, strong) NSMutableArray *toolsArray;




@property BOOL zoomIn;
@property BOOL pickMode;
@property BOOL fillMode;
@property BOOL thickPenMode;

@property (nonatomic) NSInteger cellSize;
@property (nonatomic) NSInteger gridX;
@property (nonatomic) NSInteger gridY;

@property (nonatomic) NSInteger insideX;
@property (nonatomic) NSInteger insideY;


@property (nonatomic) CGFloat xCount;
@property (nonatomic) CGFloat yCount;


@property (nonatomic, strong) UIColor *selectedColor;
@property (nonatomic) int selectedColorRed;
@property (nonatomic) int selectedColorGreen;
@property (nonatomic) int selectedColorBlue;
@property (nonatomic) int selectedColorAlpha;



@property (weak, nonatomic) IBOutlet UIView *zoomLevelButtonsView;
@property (weak, nonatomic) IBOutlet UIButton *zoomLevelButton1;
@property (weak, nonatomic) IBOutlet UIButton *zoomLevelButton2;
@property (weak, nonatomic) IBOutlet UIButton *zoomLevelButton3;
@property (weak, nonatomic) IBOutlet UIButton *zoomLevelButton4;
@property (weak, nonatomic) IBOutlet UIButton *zoomLevelButton5;



@end
