//
//  ColorBytePicker.h
//  tmp
//
//  Created by Mikhail Baynov on 13/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ColorBytePicker;


@protocol ColorBytePickerDelegate <NSObject>

- (void)colorBytePicker:(ColorBytePicker *)colorBytePicker valueSet:(NSInteger)value;

@end





@interface ColorBytePicker : UIView

@property (strong, nonatomic) id<ColorBytePickerDelegate> delegate;
@property (strong, nonatomic) UILabel * valueLabel;
@property (nonatomic) NSInteger value;
@property (nonatomic) NSInteger color;

@end
