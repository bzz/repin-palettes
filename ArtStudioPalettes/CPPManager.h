//
//  CPPManager.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 24/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//


#import <Foundation/Foundation.h>


#if defined __cplusplus
class CPPClass;						// forward class declaration
#else
typedef struct CPPClass CPPClass;   // forward struct declaration
#endif




@interface CPPManager : NSObject
{
@private
    CPPClass* cpp;
}


//@property (strong, nonatomic) NSMutableData *data;

- (NSMutableArray *)dataPaletteDataFromImage:(UIImage *)image;
- (UIImage*)mergeColorsForPicture:(UIImage*)picture newColor:(UIColor*)newColor colorsArray:(NSArray*)colorsArray;
- (UIImage*)pastePictureArea:(UIImage*)pictureArea intoPicture:(UIImage*)picture X:(int)x Y:(int)y;
- (UIImage*)cropAddPixelsToPicture:(UIImage*)picture left:(int)left right:(int)right top:(int)top bottom:(int)bottom;

- (UIImage*)flipPicture:(UIImage*)picture;
- (UIImage*)rotatePicture:(UIImage*)picture;


- (UIImage*)applyTransform:(NSInteger)transform toPicture:(UIImage*)picture;


@end