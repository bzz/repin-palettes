//
//  pixel_data.cpp
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 20/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#include <iostream>
#include <vector>
#include <algorithm>

#include "pixel_data.h"

CPPClass::CPPClass()
{
}

#pragma mark
#pragma mark
#pragma mark Pic class constructors






Pic::Pic(size_t w, size_t h) :width(w), height(h), dsize(w * h * sizeof(Color))
{
#warning get rid of operator new
	data = new Color[w * h];
//	data = std::unique_ptr<Color> (new Color[w*h];
}


Pic::Pic(size_t w, size_t h, Color* pd): Pic(w,h)
{
	data = pd;
}





#pragma mark
#pragma mark
#pragma mark


#pragma mark
#pragma mark
#pragma mark  color class operators override

std::ostream& operator<< (std::ostream &stream, Color const &c)
{
	stream << "COLOR:["<< (int)c.red << " " << (int)c.green << " " << (int)c.blue << " " << (int)c.alpha << "]  ";
	return stream;
}
inline bool operator== (const Color &lhs, const Color &rhs)
{
	if (lhs.red == rhs.red && lhs.green == rhs.green && lhs.blue == lhs.blue && lhs.alpha == lhs.alpha) return true;
	return false;
}
inline bool operator!= (const Color &lhs, const Color &rhs)
{
	if (lhs.red != rhs.red || lhs.green != rhs.green || lhs.blue != lhs.blue || lhs.alpha != lhs.alpha) return true;
	return false;
}


inline bool operator< (const Color &l, const Color &r)
{
	unsigned int *ltotal =  new unsigned int (0);
	*ltotal |= ( l.red   << 24);
	*ltotal |= ( l.green << 16);
	*ltotal |= ( l.blue  << 8 );
	*ltotal |= ( l.alpha      );
	
	unsigned int *rtotal = new unsigned int(0);
	*rtotal |= ( r.red   << 24);
	*rtotal |= ( r.green << 16);
	*rtotal |= ( r.blue  << 8 );
	*rtotal |= ( r.alpha      );
	if (*ltotal < *rtotal) {delete ltotal; delete rtotal; return true;}
		
	{delete ltotal; delete rtotal; return false;}
	return false;
}



#pragma mark
#pragma mark
#pragma mark

Line CPPClass::palette(const Pic& pic)
{
	Line invec;
	invec.clear();
	for (size_t i = 0; i<pic.width*pic.height; i++) {
		invec.emplace_back (Color{pic.data[i].red, pic.data[i].green, pic.data[i].blue, pic.data[i].alpha});
	}
	sort(invec.begin(), invec.end());
	invec.erase(unique(invec.begin(), invec.end()), invec.end());
	return invec;
}


Pic& CPPClass::merge_image_ncolor_colors(Pic& pic, Color ncolor, Line colors)
{
	Color tmpcolor;
	for (size_t i = 0; i<pic.width*pic.height; i++) {
		tmpcolor = pic.data[i];
		for (auto c: colors) {
			if (tmpcolor == c) {
				pic.data[i] = ncolor;
			}
		}
	}
	return pic;
}


Pic& CPPClass::paste_picture_area(const Pic& pic1, Pic& pic, int x, int y)
{
	for (int row=0; row<pic1.height; row++) {
		for (int i=0; i<pic1.width; i++) {
			pic.data[y*pic.width + (i+x)+row*pic.width] = pic1.data[i+row*pic1.width];
		}
	}
	return pic;
}


/// pic       picture to crop or add pixels
/// lpixel    pixels to add - left
/// rpixel    pixels to add - right
/// tpixel    pixels to add - top
/// bpixel    pixels to add - bottom
Pic& CPPClass::crop_add_pixels(Pic& pic, int lpixel, int rpixel, int tpixel, int bpixel)
{
	Pic out (pic.width + lpixel + rpixel, pic.height + tpixel + bpixel);

	Color a {255, 255, 255, 255};
	std::fill_n(out.data, out.width * out.height, a);
	
	int ttmp = (tpixel<0)?-tpixel:0;
	int btmp = (bpixel<0)?-bpixel:0;
	
	int ltmp = (lpixel<0)?-lpixel:0;
	int rtmp = (rpixel<0)?-rpixel:0;
	
	for (int row=ttmp; row<pic.height-btmp; row++) {
		for (int i=ltmp; i<pic.width-rtmp; i++) {
			out.data[row*out.width + tpixel*out.width + (i+lpixel)] = pic.data[row*pic.width + i];
		}
	}
	pic=out;
	return pic;
#warning implement background color detection func
}


Pic& CPPClass::flip_picture(Pic& pic)
{
	size_t hline = pic.width;
	
	Pic out (pic.width, pic.height);
	for (int row=0; row<pic.height; row++) {
		for (int i=0; i<pic.width; i++) {
			out.data[row*hline+i] = pic.data[row*hline+hline-i-1];
		}
	}
	pic = out;
	return pic;
}


Pic& CPPClass::rotate_picture(Pic& pic)
{
	pic = flip_picture(pic);
	size_t hline = pic.width;
	size_t vline = pic.height;
	Pic out (pic.height, pic.width);
	for (int row=0; row<hline; row++) {
		for (int i=0; i<vline; i++) {
			out.data[row*vline+i] = pic.data[row + i*hline];
		}
	}
	pic = out;
	return pic;
}




Pic& CPPClass::interpolate2x(Pic& pic)
{
	Pic out (pic.width*2, pic.height*2);
	size_t hline = out.width;
	size_t y=0;
	for (size_t row=0; row<pic.height; row++) {
		for (size_t i=0; i<pic.width; i++, y+=2) {
			auto orig = pic.data[row*pic.width+i];
			out.data[y]   = orig;
			out.data[y+1] = orig;
			out.data[y+hline]   = orig;
			out.data[y+1+hline] = orig;
		}
		y+=out.width;
	}
	pic = out;
	return pic;
}


Pic& CPPClass::downscale2x(Pic& pic)
{
	Pic out (pic.width/2, pic.height/2);
	size_t y=0;
	for (size_t row=0; row<pic.height; row+=2) {
		for (size_t i=0; i<pic.width; i+=2, y++) {
			out.data[y]  = pic.data[row*pic.width+i];
		}
	}
	pic = out;
	return pic;
}





/*
	   A    --\ 1 2
	 C P B  --/ 3 4
	   D
	 1=P; 2=P; 3=P; 4=P;
	 IF C==A AND C!=D AND A!=B => 1=A
	 IF A==B AND A!=C AND B!=D => 2=B
	 IF B==D AND B!=A AND D!=C => 4=D
	 IF D==C AND D!=B AND C!=A => 3=C
 */
Pic& CPPClass::upscale2x(Pic& pic)
{
	Pic out (pic.width*2, pic.height*2);

	Color P, A, D, C, B, one, two, three, four;
	size_t Aval, Dval, Cval, Bval;

	size_t hline = pic.width;
	size_t vline = pic.height;
	size_t size = hline*vline;
	
	for (size_t y=0; y<vline; ++y) {
		for (size_t x=0; x<hline; ++x) {
		
			P = pic.data[x+y*hline];
			
			Aval = x+y*hline-hline;
			Dval = x+y*hline+hline;
			Cval = x+y*hline-1;
			Bval = x+y*hline+1;
			
			///Validate Aval, Dval, Cval, Bval
			A = (Aval>size)? Color(): pic.data[Aval];
			D = (Dval>size)? Color(): pic.data[Dval];
			if (Cval>size || x==0) {
				C = {255,255,255,255};
			} else C = pic.data[Cval];
			if (Bval>size || x==hline-1) {
				B = {255,255,255,255};
			} else B = pic.data[Bval];

			
			C = (Cval>size)? Color{255,255,255,255}: pic.data[Cval];
			B = (Bval>size)? Color{255,255,255,255}: pic.data[Bval];
			
			one = two = three = four = P;
			if (C==A && C!=D && A!=B) { one = A;}
			if (A==B && A!=C && B!=D) { two = B;}
			if (B==D && B!=A && D!=C) { four = D;}
			if (D==C && D!=B && C!=A) { three = C;}
			
			out.data[x*2+y*4*hline] = one;
			out.data[x*2+y*4*hline+1] = two;
			out.data[x*2+y*4*hline+2*hline] = three;
			out.data[x*2+y*4*hline+2*hline+1] = four;
		}
	}
	
	pic = out;
	return pic;
}


/*
	A B C --\  1 2 3
	D E F    > 4 5 6
	G H I --/  7 8 9
	1=E; 2=E; 3=E; 4=E; 5=E; 6=E; 7=E; 8=E; 9=E;
	IF D==B AND D!=H AND B!=F => 1=D
	IF (D==B AND D!=H AND B!=F AND E!=C) OR (B==F AND B!=D AND F!=H AND E!=A) => 2=B
	IF B==F AND B!=D AND F!=H => 3=F
	IF (H==D AND H!=F AND D!=B AND E!=A) OR (D==B AND D!=H AND B!=F AND E!=G) => 4=D
	5=E
	IF (B==F AND B!=D AND F!=H AND E!=I) OR (F==H AND F!=B AND H!=D AND E!=C) => 6=F
	IF H==D AND H!=F AND D!=B => 7=D
	IF (F==H AND F!=B AND H!=D AND E!=G) OR (H==D AND H!=F AND D!=B AND E!=I) => 8=H
	IF F==H AND F!=B AND H!=D => 9=F
 */
Pic& CPPClass::upscale3x(Pic& pic)
{
	Pic out (pic.width*3, pic.height*3);
	
	Color A, B, C, D, E, F, G, H, I, p1, p2, p3, p4, p5, p6, p7, p8, p9;

	
	size_t Aval, Bval, Cval, Dval,     Fval, Gval, Hval, Ival;
	
	size_t hline = pic.width;
	size_t vline = pic.height;
	size_t size = hline*vline;

	for (size_t y=0; y<vline; ++y) {
		for (size_t x=0; x<hline; ++x) {
			
			

			E = pic.data[x+y*hline];
			
			Aval = x+y*hline-hline-1;
			Bval = x+y*hline-hline;
			Cval = x+y*hline-hline+1;

			Dval = x+y*hline-1;
			Fval = x+y*hline+1;

			Gval = x+y*hline+hline-1;
			Hval = x+y*hline+hline;
			Ival = x+y*hline+hline+1;

			///Validate Aval, Dval, Cval, Bval
			B = (Bval>size)? Color{255,255,255,255}: pic.data[Bval];
			H = (Hval>size)? Color{255,255,255,255}: pic.data[Hval];


#warning tbd validation
			A = (Aval>size)? Color{255,255,255,255}: pic.data[Aval];
			C = (Cval>size)? Color{255,255,255,255}: pic.data[Cval];
			D = (Dval>size)? Color{255,255,255,255}: pic.data[Dval];
			F = (Fval>size)? Color{255,255,255,255}: pic.data[Fval];
			G = (Gval>size)? Color{255,255,255,255}: pic.data[Gval];
			I = (Ival>size)? Color{255,255,255,255}: pic.data[Ival];
			
			p1 = p2 = p3 = p4 = p5 = p6 = p7 = p8 = p9 = E;

	
			if (D==B && D!=H && B!=F)  { p1=D; }
			if ((D==B && D!=H &&  B!=F &&  E!=C) || (B==F &&  B!=D &&  F!=H &&  E!=A)) { p2=B; }
			if (B==F && B!=D &&  F!=H) { p3=F; }
			if ((H==D && H!=F &&  D!=B &&  E!=A) || (D==B &&  D!=H &&  B!=F &&  E!=G)) {p4=D;}
//			p5 = E;
			if ((B==F && B!=D && F!=H &&  E!=I) ||  (F==H &&  F!=B &&  H!=D &&  E!=C)) {p6=F;}
			if (H==D && H!=F && D!=B)  { p7=D; }
			if ((F==H && F!=B &&  H!=D  && E!=G) || (H==D &&  H!=F &&  D!=B &&  E!=I)) {p8=H;}
			if (F==H && F!=B &&  H!=D) { p9=F; }

			out.data[x*3+y*9*hline]   = p1;
			out.data[x*3+y*9*hline+1] = p2;
			out.data[x*3+y*9*hline+2] = p3;
			out.data[x*3+y*9*hline +3*hline]   = p4;
			out.data[x*3+y*9*hline +3*hline+1] = p5;
			out.data[x*3+y*9*hline +3*hline+2] = p6;
			out.data[x*3+y*9*hline +6*hline]   = p7;
			out.data[x*3+y*9*hline +6*hline+1] = p8;
			out.data[x*3+y*9*hline +6*hline+2] = p9;
		}
	}

	pic = out;
	return pic;
}




