//
//  ColorBytePicker.m
//  tmp
//
//  Created by Mikhail Baynov on 13/10/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#import "ColorBytePicker.h"



@interface ColorBytePicker ()
{
    NSInteger startingX;
}

@end

@implementation ColorBytePicker


- (instancetype)init {
    if (self=[super init]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self=[super initWithCoder:aDecoder]) {
        [self setupInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self=[super initWithFrame:frame]) {
        self.frame = frame;
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
    _valueLabel = [[UILabel alloc]initWithFrame:self.bounds];
    [self addSubview:_valueLabel];
    _valueLabel.textAlignment = NSTextAlignmentCenter;
    _valueLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:self.frame.size.height/1.5];
    self.value = 0;
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch= [touches anyObject];
    startingX = [touch locationInView:self].x;
    if ( startingX > self.frame.size.width/2) {
        self.value++;
    } else {
        self.value--;
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch= [touches anyObject];
    if ([touch locationInView:self].x > startingX) {
        self.value += ([touch locationInView:self].x - startingX)/self.frame.size.width*255;
    } else {
        self.value -= (startingX - [touch locationInView:self].x)/self.frame.size.width*255;
    }
    startingX = [touch locationInView:self].x;
}

- (void)setValue:(NSInteger)value {
    if (value < 0) {
        _value = 0;
    } else if (value > 255) {
        _value = 255;
    } else _value = value;
    _valueLabel.text = [NSString stringWithFormat:@"<%03i>", _value];
    
    [self.delegate colorBytePicker:self valueSet:_value];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
