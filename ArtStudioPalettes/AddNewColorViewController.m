//
//  AddNewColorViewController.m
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 17/04/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "AddNewColorViewController.h"

@interface AddNewColorViewController ()

@end

@implementation AddNewColorViewController


#pragma mark
#pragma mark
#pragma mark INIT
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}

- (void)setupInit {
}

#pragma mark
#pragma mark
#pragma mark LIFECYCLE

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
//	[self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (BOOL)prefersStatusBarHidden {
	return YES;
}




#pragma mark
#pragma mark
#pragma mark COLLECTION VIEW DELEGATES

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
}



#pragma mark
#pragma mark
#pragma mark ACTIONS
- (IBAction)yesButtonPressed:(id)sender {
}
- (IBAction)noButtonPressed:(id)sender {
}


#pragma mark
#pragma mark
#pragma mark PREPARE FOR SEGUE

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
//	[self.navigationController setNavigationBarHidden:NO animated:YES];
}


@end
