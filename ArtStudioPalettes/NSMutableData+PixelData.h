//
//  NSMutableData+PixelData.h
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/25/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableData (PixelData)

+ (NSMutableData *)dataFromImage:(UIImage *)image;

+ (NSMutableData *)dataChangePixelColorRed:(int)red Green:(int)green Blue:(int)blue Alpha:(int)alpha forX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image;
+ (NSMutableData *)dataFillWithColorRed:(int)red Green:(int)green Blue:(int)blue Alpha:(int)alpha fromX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image;
+ (NSMutableData *)dataApplyRed:(CGFloat)red Green:(CGFloat)green Blue:(CGFloat)blue Alpha:(CGFloat)alpha toData:(NSMutableData *)data forImage:(UIImage *)image;
+ (NSMutableData *)blackWhiteSketchFromImage:(UIImage*)image;



#warning UNUSED! scale2x
//Scaling methods
+ (NSMutableData *)dataApplyScaleInterpolation2x:(NSMutableData *)data destinationImage:(UIImage *)image;
+ (NSMutableData *)dataApplyEPXScale2x:(NSMutableData *)data destinationImage:(UIImage *)image;

+ (NSMutableData *)dataContourFillWithColorRed:(int)red Green:(int)green Blue:(int)blue Alpha:(int)alpha fromX:(int)x Y:(int)y ofData:(NSMutableData *)data forImage:(UIImage *)image;



@end
