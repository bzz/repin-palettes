//
//  Color.h
//  ArtStudioPalettes
//
//  Created by Mikhail Baynov on 06/12/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Color : NSObject <NSCoding>

@property (nonatomic) NSInteger red;
@property (nonatomic) NSInteger green;
@property (nonatomic) NSInteger blue;
@property (nonatomic) NSInteger alpha;


- (instancetype)initWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha;
- (UIColor *)uicolor;
- (NSString *)description;

- (NSInteger)distanceFromColor:(Color *)color;
- (NSInteger)placeInRow;

@end
